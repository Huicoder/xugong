import datetime
import numpy as np
import pandas as pd
import random
from components.packages.platgo.Population import Population
import json
import copy


def process_data_set(data_set):
    device_string2index = dict()
    worker_string2index = dict()
    device_index2string = dict()
    worker_index2string = dict()
    device_index = 1
    worker_index = 0
    tasks = len(data_set["scheduling_task"])

    for item in data_set["scheduling_task"]:
        for i, device_string in enumerate(item["available_device"]):
            if device_string not in device_string2index:
                device_string2index[device_string] = []
                device_index2string[str(device_index)] = []

                device_value = item["available_device"][device_string]
                for k in range(device_index, device_index + device_value):
                    device_string2index[device_string] += [k]
                    device_index2string[str(k)] = device_string
                    device_index += 1
            item["available_device"][device_string] = [
                str(i) for i in device_string2index[device_string]
            ]
        item["available_device"] = sum(
            list(map(lambda x: x, item["available_device"].values())), []
        )
        for i, worker_string in enumerate(item["available_worker"]):
            if worker_string not in worker_string2index:
                worker_string2index[worker_string] = []
                worker_index2string[str(worker_index)] = []

                worker_value = item["available_worker"][worker_string]
                for k in range(worker_index, worker_index + worker_value):
                    worker_string2index[worker_string] += [k]
                    worker_index2string[str(k)] = worker_string
                    worker_index += 1
            item["available_worker"][worker_string] = [
                str(i) for i in worker_string2index[worker_string]
            ]
        # 使用lambda函数结合map()方法获取字典中的所有值,并进行列表合并

        item["available_worker"] = sum(
            list(map(lambda x: x, item["available_worker"].values())), []
        )

    for item in data_set["scheduled_task"]:
        for i, device_string in enumerate(item["using_device"]):
            if device_string not in device_string2index:
                device_string2index[device_string] = []
                device_index2string[str(device_index)] = []
                device_value = item["using_device"][device_string]
                for k in range(device_index, device_index + device_value):
                    device_string2index[device_string] += [k]
                    device_index2string[str(k)] = device_string
                    device_index += 1

            item["using_device"] = [
                str(int(random.choice(device_string2index[device_string])))
            ]

        for i, worker_string in enumerate(item["using_worker"]):
            if worker_string not in worker_string2index:
                worker_string2index[worker_string] = []
                worker_index2string[str(worker_index)] = []
                worker_value = item["using_worker"][worker_string]
                for k in range(worker_index, worker_index + worker_value):
                    worker_string2index[worker_string] += [k]
                    worker_index2string[str(k)] = worker_string
                    worker_index += 1
            item["using_worker"] = [
                str(int(random.choice(worker_string2index[worker_string])))
            ]

    return {
        "deviceIndex2String": device_index2string,
        "workerIndex2String": worker_index2string,
        "deviceString2Index": device_string2index,
        "workerString2Index": worker_string2index,
        "optimizationProblem": {
            "n_var": tasks * 2,
            "lower": [1, 0] * tasks,
            "upper": [device_index, worker_index] * tasks,
            "dataSet": [data_set],
            "algoResultType": 0,
        },
    }


def init_pop(data):
    dec = []
    N = 100
    for k in range(N):
        dec1 = []
        for i in range(len(data['scheduling_task'])):
            # 给第i个实验随机分配一个符合要求的实验台
            experiment_i = np.random.choice(data['scheduling_task'][i]['available_device'])
            # 给第i个实验根据已经分配的实验台再随机分配一个符合要求的实验员
            pop_i = np.random.choice(data['scheduling_task'][i]['available_worker'])
            dec1 += [experiment_i, pop_i]
        dec += [dec1]
    dec = np.array(dec, dtype=np.float64)
    return Population(decs=dec)


def pan(
        device,
        worker,
        data,
        using_worker,
        using_device,
        init_time,
        time_tmp,
        time1,
        task_type,
        time_list,
        pop_dict,
        worker_string2index,
        worker_index2string,
        pre
):
    """
    判断待排任务与固定任务是否冲突，时间冲突则进行调整
    :param device: 待排任务占用的设备id
    :param worker: 待排任务占用的人员id
    :param data: 数据集
    :param using_worker: 每个固定任务所需人员id
    :param using_device: 每个固定任务所需设备id
    :param init_time: 数据集中给出的开始时间
    :param time_tmp: 该待排任务的结束时间
    :param time1: 该待排任务的实验时长
    :param task_type: 实验类型
    :param time_list: # 每个设备的使用时间段
    :param pop_dict:  # 每个人员的占用时间段
    :param worker_string2index
    :param worker_index2string
    :param pre
    :return: 该待排任务的在解决冲突后的开始时间和结束时间
    """
    time = []
    if pre:  # 当前待排任务顺序靠后
        time = pre  # 将顺序在前的任务的时间加入
    # 在固定任务中找出与当前待排任务相同的人员的开始时间和结束时间
    # 其实也在固定任务中找出当前待排任务所需人员的所有的工作时间段
    if task_type == "可靠性":
        for i in np.where(using_worker == str(int(worker)))[0]:
            time.append(
                [
                    data["scheduled_task"][i]["start_time"],
                    data["scheduled_task"][i]["end_time"],
                ]
            )
    else:
        for j in worker_string2index[worker_index2string[str(int(worker))]]:
            for i in np.where(using_worker == str(int(j)))[0]:
                time.append(
                    [
                        data["scheduled_task"][i]["start_time"],
                        data["scheduled_task"][i]["end_time"],
                    ]
                )

    # 在固定任务中找出与当前待排任务相同的设备的开始时间和结束时间
    # 其实也在固定任务中找出当前待排任务所需设备的所有工作时间段
    for i in np.where(using_device == str(int(device)))[0]:
        time.append(
            [
                data["scheduled_task"][i]["start_time"],
                data["scheduled_task"][i]["end_time"],
            ]
        )
    if str(int(device)) in time_list:
        for i in time_list[str(int(device))]:
            time.append(i)
    if str(int(worker)) in pop_dict:
        for i in pop_dict[str(int(worker))]:
            time.append(i)
    time.sort(key=lambda x: (x[0], x[1]))
    flag = True  # 判断待排任务是否固定 True为不固定
    # 根据时间表调整当前待排任务的开始时间和结束时间
    init_time, time_tmp = juddge(init_time, time_tmp, time1)
    for i in range(len(time)):
        # 判断当前的待排任务与固定任务的时间是否冲突
        if (
                time[i][0] < init_time < time[i][1]
                or time[i][0] < str(time_tmp) < time[i][1]
        ):
            init_time = time[i][1]  # 时间冲突，将待排任务的开始时间设置发生冲突固定任务的结束时间
        elif init_time <= time[i][0] and str(time_tmp) >= time[i][1]:
            init_time = time[i][1]  # 时间冲突，将待排任务的开始时间设置发生冲突固定任务的结束时间
        elif str(time_tmp) <= time[i][0]:  # 待排任务的结束时间在固定任务开始时间之前，此时待排任务的位置就可以固定了
            flag = False  # 待排任务和固定任务没有发生时间冲突
        # 解决时间冲突后，计算当前待排任务的实际的开始时间和结束时间
        init_time, time_tmp = juddge(init_time, time_tmp, time1)
        if not flag:  # flag为False说明待排任务已经解决与固定任务的时间冲突，且位置已经固定
            break
    return init_time, time_tmp


def juddge(init_time, time_tmp, time1):
    """
    根据时间表调整待排任务的开始时间和结束时间
    :param init_time: 待排任务的开始时间
    :param time_tmp: 待排任务的结束时间
    :param time1:  待排任务的实验时间
    :return:  调整后的开始时间和结束时间
    """
    if "02-16" <= init_time[5:10] <= "07-14":
        if (  # noqas
                "08:30:00" <= init_time[11:] < "12:00:00"
                or "13:30:00" <= init_time[11:] < "18:00:00"
        ):
            init_time = init_time
        elif "12:00:00" <= init_time[11:] < "13:30:00":
            init_time = init_time[:11] + "13:30:00"
        elif "00:00:00" <= init_time[11:] < "08:30:00":
            init_time = init_time[:11] + "08:30:00"
        else:
            init_time = (
                    str(
                        datetime.datetime.strptime(init_time[:10], "%Y-%m-%d")
                        + datetime.timedelta(days=1)
                    )[:11]
                    + "08:30:00"
            )
    elif "07-15" <= init_time[5:10] <= "09-30":
        if (
                "08:30:00" <= init_time[11:] < "12:00:00"
                or "14:00:00" <= init_time[11:] < "18:00:00"
        ):
            init_time = init_time
        elif "12:00:00" <= init_time[11:] < "14:00:00":
            init_time = init_time[:11] + "14:00:00"
        elif "00:00:00" <= init_time[11:] < "08:30:00":
            init_time = init_time[:11] + "08:30:00"
        else:
            init_time = (
                    str(
                        datetime.datetime.strptime(init_time[:10], "%Y-%m-%d")
                        + datetime.timedelta(days=1)
                    )[:11]
                    + "08:30:00"
            )
    elif "10-01" <= init_time[5:10] <= "11-15":
        if (
                "08:30:00" <= init_time[11:] < "12:00:00"
                or "13:30:00" <= init_time[11:] < "18:00:00"
        ):
            init_time = init_time
        elif "12:00:00" <= init_time[11:] < "13:30:00":
            init_time = init_time[:11] + "13:30:00"
        elif "00:00:00" <= init_time[11:] < "08:30:00":
            init_time = init_time[:11] + "08:30:00"
        else:
            init_time = (
                    str(
                        datetime.datetime.strptime(init_time[:10], "%Y-%m-%d")
                        + datetime.timedelta(days=1)
                    )[:11]
                    + "08:30:00"
            )
    else:
        if (
                "08:30:00" <= init_time[11:] < "12:00:00"
                or "13:30:00" <= init_time[11:] < "17:30:00"
        ):
            init_time = init_time
        elif "12:00:00" <= init_time[11:] < "13:30:00":
            init_time = init_time[:11] + "13:30:00"
        elif "00:00:00" <= init_time[11:] < "08:30:00":
            init_time = init_time[:11] + "08:30:00"
        else:
            init_time = (
                    str(
                        datetime.datetime.strptime(init_time[:10], "%Y-%m-%d")
                        + datetime.timedelta(days=1)
                    )[:11]
                    + "08:30:00"
            )
    time_tmp = datetime.datetime.strptime(
        init_time, "%Y-%m-%d %H:%M:%S"
    ) + datetime.timedelta(minutes=time1)
    return init_time, time_tmp


def count_Need_PeopleAndAdvice(data):
    """
    统计固定任务和待排任务所需人员和设备的id
    :param data: 数据集
    :return: 人员id和设备id（去重后的）
    """
    available_device = []
    available_pop = []
    i = max(len(data["scheduling_task"]), len(data["scheduled_task"]))
    for k in range(i):
        if k < len(data["scheduling_task"]):
            available_device.append(
                data["scheduling_task"][k]["available_device"]
            )
            available_pop.append(data["scheduling_task"][k]["available_worker"])
        if k < len(data["scheduled_task"]):
            available_pop.append(data["scheduled_task"][k]["using_worker"])
            available_device.append(data["scheduled_task"][k]["using_device"])
    available_device = list(set(sum(available_device, [])))  # 降维->去重->转为列表
    available_pop = list(set(sum(available_pop, [])))  # sum(x,[]) 也可以用于列表的展开，效果相当于各子列表相加(降维)
    return available_device, available_pop


def find_EarliestTime_InSameExperiment(data, x):
    """
    找出相同项目下的最早开始的任务时间和优先级
    :param data: 数据集
    :param x: 决策变量
    :return: 相同项目下的最早开始的任务时间和优先级
    """
    D = len(x)
    experiment_id = {}  # {'1088939404472352768': [('2023-03', 5)]} 项目id，一个项目下会有多个任务id
    if len(data["scheduling_task"]) != 0:
        if data["scheduling_task"][0]["laboratory"] == "CD":  # 判断待排任务是否属于传动实验室
            for j in range(0, D, 2):  # 对决策变量的维度循环（实际上对待排任务进行遍历）
                if "plan_start_time" in data["scheduling_task"][int(j / 2)]:  # 有计划开始时间的待排任务
                    tt = data["scheduling_task"][int(j / 2)]["experiment_id"]
                    if data["scheduling_task"][int(j / 2)]["experiment_id"] not in experiment_id:
                        experiment_id.setdefault(tt, []).append(
                            (
                                data["scheduling_task"][int(j / 2)]["plan_start_time"][0:7],
                                data["scheduling_task"][int(j / 2)]["task_order"],
                            )
                        )
                    else:  # 是一个相同项目下的待排任务
                        if (
                                data["scheduling_task"][int(j / 2)]["plan_start_time"][0:7] <
                                experiment_id[data["scheduling_task"][int(j / 2)]["experiment_id"]][0][0]
                        ):  # 在同一个项目下当前待排任务的开始时间先于前一个待排任务的
                            tt = data["scheduling_task"][int(j / 2)]["experiment_id"]
                            experiment_id[tt] = [
                                (
                                    data["scheduling_task"][int(j / 2)]["plan_start_time"][0:7],
                                    data["scheduling_task"][int(j / 2)]["task_order"],
                                )
                            ]
                else:  # 没有计划开始时间的待排任务
                    tt = data["scheduling_task"][int(j / 2)]["experiment_id"]
                    if data["scheduling_task"][int(j / 2)]["experiment_id"] not in experiment_id:
                        experiment_id.setdefault(tt, []).append(
                            ("3000-13", data["scheduling_task"][int(j / 2)]["task_order"])
                        )
    return experiment_id


def find_Same_ExperimentId_Task(data):
    same_experimentid_task = {}
    for item in data["scheduling_task"]:
        same_experimentid_task.setdefault(item["experiment_id"], []).append((item["task_id"], item["task_sort"]))
    return same_experimentid_task


def group_TaskToAdvice(data, available_device, experiment_id, x):
    """
    由于设备是可以并行工作，先将每个任务按照设备分为各个组
    :param data: 数据集
    :param available_device: 使用的设备集合
    :param experiment_id: 每个项目下的任务的最早开始时间和优先级
    :param x: 决策变量
    :return: 分组后的人员使用表，任务顺序表，实验时间表
    """
    pop_i = []  # 同一设备下的人员id
    experiment = []  # 同一设备下任务顺序
    time = []  # 同一设备下每个任务的实验时间
    # 将每个任务按照设备分为各个组
    for k in range(1, len(available_device) + 1):  # 对每个设备进行循环
        experiment_time = []  # 同一项目下实验时间
        people = []  # 同一项目下的人员顺序
        order = []  # 同一项目下的任务优先级顺序
        seq1 = []  # 同一项目下的任务开始时间和优先级[('2023-03', 5, '1088939404472352768')]
        t = []  # json文件中的实验顺序  从0开始
        # 先遍历所有任务，将其计划时间、任务优先级都修改同一个项目下最高的那一个，
        # 以保证一个项目下的所有任务排序时都将连续
        for j in range(0, len(x), 2):  # 对待排任务循环
            if x[j] == k:  # 判断设备是否是所需设备
                people.append(x[j + 1])  # 如果是所需设备，则进行人员添加
                experiment_time.append(
                    data["scheduling_task"][int(j / 2)]["task_duration"]
                )
                tt = data["scheduling_task"][int(j / 2)]["experiment_id"]
                if "plan_start_time" in data["scheduling_task"][int(j / 2)]:  # 有计划开始时间的待排任务
                    if data["scheduling_task"][int(j / 2)]["laboratory"] == "CD":  # 该待排任务属于传动实验室
                        # experiment_id[tt]->[('2023-03', 5)]
                        seq1.append((data['scheduling_task'][int(j / 2)]['task_sort'], experiment_id[tt][0][0],
                                     experiment_id[tt][0][1], tt))
                    else:  # 该待排任务属于普通实验室
                        seq1.append((data['scheduling_task'][int(j / 2)]['task_sort'],
                                     data['scheduling_task'][int(j / 2)]['plan_start_time'][0:7],  # noqa
                                     data['scheduling_task'][int(j / 2)]['task_order'], tt))  # noqa
                else:  # 没有计划开始时间的待排任务
                    if data["scheduling_task"][int(j / 2)]["laboratory"] == "CD":  # 该待排任务属于传动实验室
                        seq1.append(
                            (data['scheduling_task'][int(j / 2)]['task_sort'], "3000-13", experiment_id[tt][0][1], tt))
                    else:  # 该待排任务属于普通实验室
                        seq1.append((data['scheduling_task'][int(j / 2)]['task_sort'], "3000-13",
                                     data['scheduling_task'][int(j / 2)]['task_order'], tt))
                order.append(data["scheduling_task"][int(j / 2)]["task_order"])
                t.append(int(j / 2))
        # 根据元组第0个值升序排序，若第0个值相等则根据第1个值升序排序
        # 先按照年月份排序再按照优先级排序
        tmp1 = sorted(range(len(t)), key=lambda x: (seq1[x][0], seq1[x][1], seq1[x][2]))
        pop_i.append(np.array(people)[tmp1])
        experiment.append(np.array(t)[tmp1])
        time.append(np.array(experiment_time)[tmp1])
    pop_i = pd.DataFrame(pop_i)
    experiment = pd.DataFrame(experiment)
    time = pd.DataFrame(time)
    return pop_i, experiment, time


def set_SchedulingTaskTime(data, init_time, pop_i, experiment, time, worker_index2string, worker_string2index,
                           same_experiment_id_task):
    """
    固定待排任务的在各自设备上的开始时间和结束时间
    :param data: 数据集
    :param init_time: 开始时间
    :param pop_i: 人员表
    :param experiment: 任务表
    :param time: 实验时间表
    :param worker_index2string:
    :param worker_string2index:
    :param same_experiment_id_task
    :return: 每个设备及人员的占用时间段
    """
    time_list = {}  # 记录每个设备的占用时间段{'1': [['2023-03-30 10:20:00', '2023-03-30 11:10:00']]}
    pop_dict = {}  # 记录每个人员的占用时间段{'2': [['2023-03-30 10:20:00', '2023-03-30 11:10:00']]}
    experiment_done = {}  # 判断实验是否做过
    Flag = {}  # 存放已做实验的时间(key值为task_id)
    Flag_c = [0] * experiment.shape[0]  # 存放设备的第几个实验
    using_worker = []  # 固定任务所需人员 [list(['0']) list(['1']) list(['2'])]->['0' '1' '2']
    using_device = []  # 固定任务所需设备 [list(['1']) list(['2']) list(['1'])]->['1' '2' '1']
    if len(data["scheduled_task"]):  # 判断是否有固定任务
        using_worker = np.array(
            pd.DataFrame(data["scheduled_task"])["using_worker"]
        )
        using_device = np.array(
            pd.DataFrame(data["scheduled_task"])["using_device"]
        )
    for k in range(len(using_device)):  # len的长度就是固定任务的长度
        using_worker[k] = using_worker[k][0]
        using_device[k] = using_device[k][0]
    last_col = experiment.shape[1]
    experiment[last_col] = np.nan
    while len(experiment_done) != len(data["scheduling_task"]):  # 确保所有的实验全部做完
        for j in range(experiment.shape[0]):  # 对每个设备进行遍历
            c = Flag_c[j]  # 存放当前设备的第几个实验
            if ~np.isnan(experiment[c][j]):
                task_sort = data["scheduling_task"][int(experiment[c][j])]["task_sort"]  # 待排任务的顺序
                task_list = []
                if task_sort > 1:  # 待排任务顺序靠后
                    task_flag = True  # 判断任务顺序合规
                    for item in same_experiment_id_task[
                        data["scheduling_task"][int(experiment[c][j])]["experiment_id"]]:
                        if item[1] < task_sort:  # 找到顺序在前的项目的id
                            task_list.append(item[0])
                    for tid in task_list:
                        if tid not in Flag:  # 顺序在前的项目没有做
                            task_flag = False  # 不合规
                    if not task_flag:  # 不合规跳过该项目
                        continue
                if c == 0:  # 先看每个设备上的第一个项目
                    if str(int(pop_i[c][j])) not in pop_dict:  # 该待排任务的所需人员不在pop_dict中
                        init_time1 = init_time
                    else:
                        if data["scheduling_task"][int(experiment[c][j])]["task_type"] == "性能":
                            init_time1 = init_time
                            for i in worker_string2index[worker_index2string[str(int(pop_i[c][j]))]]:
                                if str(i) in pop_dict:
                                    if pop_dict[str(i)][-1][1] > init_time1:
                                        init_time1 = pop_dict[str(i)][-1][1]
                        else:
                            init_time1 = pop_dict[str(int(pop_i[c][j]))][-1][1]
                    pre = []  # 顺序在前待排任务的开始和结束时间
                    if len(task_list) != 0:
                        for tid in task_list:
                            pre.append(Flag[tid])
                        pre.sort(key=lambda x: (x[0], x[1]))
                        init_time1 = max(pre[-1][1], init_time1)
                    time_tmp = datetime.datetime.strptime(
                        init_time1, "%Y-%m-%d %H:%M:%S"
                    ) + datetime.timedelta(
                        minutes=data["scheduling_task"][
                            int(experiment[c][j])
                        ]["task_duration"]
                    )
                else:  # 再看场地的第二个及以后的实验
                    if str(int(pop_i[c][j])) not in pop_dict:
                        init_time1 = time_list[str(j + 1)][-1][1]
                    else:
                        if data["scheduling_task"][int(experiment[c][j])]["task_type"] == "性能":
                            init_time1 = init_time
                            for i in worker_string2index[worker_index2string[str(int(pop_i[c][j]))]]:
                                if str(i) in pop_dict:
                                    if pop_dict[str(i)][-1][1] > init_time1:
                                        init_time1 = pop_dict[str(i)][-1][1]
                        else:
                            init_time1 = pop_dict[str(int(pop_i[c][j]))][-1][1]
                        init_time1 = max(
                            time_list[str(j + 1)][-1][1],
                            init_time1,
                        )
                        pre = []  # 顺序在前待排任务的开始和结束时间
                        if len(task_list) != 0:
                            for tid in task_list:
                                pre.append(Flag[tid])
                            pre.sort(key=lambda x: (x[0], x[1]))
                            init_time1 = max(pre[-1][1], init_time1)
                    time_tmp = datetime.datetime.strptime(
                        init_time1, "%Y-%m-%d %H:%M:%S"
                    ) + datetime.timedelta(
                        minutes=data["scheduling_task"][
                            int(experiment[c][j])
                        ]["task_duration"]
                    )
                Flag_c[j] += 1  # 第j个设备的项目加1

                init_time1, time_tmp = pan(
                    j + 1,
                    pop_i[c][j],
                    data,
                    using_worker,
                    using_device,
                    init_time1,
                    time_tmp,
                    data["scheduling_task"][int(experiment[c][j])][
                        "task_duration"
                    ],
                    data["scheduling_task"][int(experiment[c][j])][
                        "task_type"
                    ],
                    time_list,
                    pop_dict,
                    worker_string2index,
                    worker_index2string,
                    pre
                )
                # 更新设备占用表
                time_list.setdefault(str(int(j + 1)), []).append(
                    [init_time1, str(time_tmp)]
                )
                # 更新人员占用表
                if data["scheduling_task"][int(experiment[c][j])]["task_type"] == "性能":
                    for i in worker_string2index[worker_index2string[str(int(pop_i[c][j]))]]:
                        pop_dict.setdefault(str(i), []).append(
                            [init_time1, str(time_tmp)]
                        )
                else:
                    pop_dict.setdefault(str(int(pop_i[c][j])), []).append(
                        [init_time1, str(time_tmp)]
                    )
                # 记录已做的实验
                experiment_done[str(int(experiment[c][j]))] = True
                Flag[
                    data["scheduling_task"][int(experiment[c][j])]["task_id"]
                ] = [init_time1, str(time_tmp)]
    return time_list, pop_dict, Flag

    # for h in range(time.shape[1]):  # 根据设备将待排任务分组后形成的表的列数
    #     s = 1  # 记录设备id
    #     for h1 in range(time.shape[0]):  # 并行设备的个数
    #         if h == 0:  # 从第一列开始计算每个任务的开始时间
    #             if ~np.isnan(pop_i[h][h1]):
    #                 if str(int(pop_i[h][h1])) not in pop_dict:  # 该待排任务的所需人员不在pop_dict中
    #                     init_time1 = init_time
    #                 else:
    #                     if data["scheduling_task"][int(experiment[h][h1])]["task_type"] == "性能":
    #                         init_time1 = init_time
    #                         for i in worker_string2index[worker_index2string[str(int(pop_i[h][h1]))]]:
    #                             if str(i) in pop_dict:
    #                                 if pop_dict[str(i)][-1][1] > init_time1:
    #                                     init_time1 = pop_dict[str(i)][-1][1]
    #                     else:
    #                         init_time1 = pop_dict[str(int(pop_i[h][h1]))][-1][1]
    #                 time_tmp = datetime.datetime.strptime(
    #                     init_time1, "%Y-%m-%d %H:%M:%S"
    #                 ) + datetime.timedelta(
    #                     minutes=data["scheduling_task"][
    #                         int(experiment[h][h1])
    #                     ]["task_duration"]
    #                 )
    #                 init_time1, time_tmp = pan(
    #                     h1 + 1,
    #                     pop_i[h][h1],
    #                     data,
    #                     using_worker,
    #                     using_device,
    #                     init_time1,
    #                     time_tmp,
    #                     data["scheduling_task"][int(experiment[h][h1])][
    #                         "task_duration"
    #                     ],
    #                     data["scheduling_task"][int(experiment[h][h1])][
    #                         "task_type"
    #                     ],
    #                     time_list,
    #                     pop_dict,
    #                     worker_string2index,
    #                     worker_index2string
    #                 )
    #                 time_list.setdefault(str(int(s)), []).append(
    #                     [init_time1, str(time_tmp)]
    #                 )
    #                 if data["scheduling_task"][int(experiment[h][h1])]["task_type"] == "性能":
    #                     for i in worker_string2index[worker_index2string[str(int(pop_i[h][h1]))]]:
    #                         pop_dict.setdefault(str(i), []).append(
    #                             [init_time1, str(time_tmp)]
    #                         )
    #                 else:
    #                     pop_dict.setdefault(str(int(pop_i[h][h1])), []).append(
    #                         [init_time1, str(time_tmp)]
    #                     )
    #             s += 1
    #         else:
    #             if ~np.isnan(pop_i[h][h1]):
    #                 if str(int(pop_i[h][h1])) not in pop_dict:
    #                     init_time1 = time_list[str(s)][-1][1]
    #                 else:
    #                     if data["scheduling_task"][int(experiment[h][h1])]["task_type"] == "性能":
    #                         init_time1 = init_time
    #                         for i in worker_string2index[worker_index2string[str(int(pop_i[h][h1]))]]:
    #                             if str(i) in pop_dict:
    #                                 if pop_dict[str(i)][-1][1] > init_time1:
    #                                     init_time1 = pop_dict[str(i)][-1][1]
    #                     else:
    #                         init_time1 = pop_dict[str(int(pop_i[h][h1]))][-1][1]
    #                     init_time1 = max(
    #                         time_list[str(s)][-1][1],
    #                         init_time1,
    #                     )
    #                 time_tmp = datetime.datetime.strptime(
    #                     init_time1, "%Y-%m-%d %H:%M:%S"
    #                 ) + datetime.timedelta(
    #                     minutes=data["scheduling_task"][
    #                         int(experiment[h][h1])
    #                     ]["task_duration"]
    #                 )
    #                 init_time1, time_tmp = pan(
    #                     h1 + 1,
    #                     pop_i[h][h1],
    #                     data,
    #                     using_worker,
    #                     using_device,
    #                     init_time1,
    #                     time_tmp,
    #                     data["scheduling_task"][int(experiment[h][h1])][
    #                         "task_duration"
    #                     ],
    #                     data["scheduling_task"][int(experiment[h][h1])][
    #                         "task_type"
    #                     ],
    #                     time_list,
    #                     pop_dict,
    #                     worker_string2index,
    #                     worker_index2string
    #                 )
    #                 time_list.setdefault(str(int(s)), []).append(
    #                     [init_time1, str(time_tmp)]
    #                 )
    #                 if data["scheduling_task"][int(experiment[h][h1])]["task_type"] == "性能":
    #                     for i in worker_string2index[worker_index2string[str(int(pop_i[h][h1]))]]:
    #                         pop_dict.setdefault(str(i), []).append(
    #                             [init_time1, str(time_tmp)]
    #                         )
    #                 else:
    #                     pop_dict.setdefault(str(int(pop_i[h][h1])), []).append(
    #                         [init_time1, str(time_tmp)]
    #                     )
    #             s += 1


def cal_Toaltime(init_time, time_list, available_device):
    """
    计算最终目标时间，即最后一个待排任务的结束时间
    :param init_time: 数据集给出的开始时间
    :param time_list: 每个设备的占用时间段
    :param available_device: 记录设备的id的列表
    :return: 结束时间
    """
    f = init_time
    for q in range(1, len(available_device) + 1):
        if str(q) in time_list:
            if f <= time_list[str(q)][-1][1]:
                f = time_list[str(q)][-1][1]
    f = datetime.datetime.strptime(f, "%Y-%m-%d %H:%M:%S") - datetime.datetime.strptime(init_time,
                                                                                        "%Y-%m-%d %H:%M:%S")
    f = f.days * 24 * 3600 + f.seconds  # 秒当作目标值,从初始时间到最后一个待排任务的结束时间
    return f


def main(data_set):
    # 存储固定任务源格式
    outcome_scheduled = copy.deepcopy(data_set["scheduled_task"])
    for item in outcome_scheduled:
        item["using_worker"] = list(item["using_worker"].keys())
        item["using_device"] = list(item["using_device"].keys())
    # 数据预处理
    preprocessing_res = process_data_set(data_set)
    device_index2string = preprocessing_res["deviceIndex2String"]
    worker_index2string = preprocessing_res["workerIndex2String"]
    worker_string2index = preprocessing_res["workerString2Index"]
    # 读取数据集
    data = preprocessing_res["optimizationProblem"]["dataSet"][0]
    # 初始化种群
    pop = init_pop(data)
    # 选择种群中的第一个个体（序列）
    # pop.decs = [
    #     [4., 3., 10., 0., 8., 4., 1., 3., 12., 1., 11., 1., 6., 5., 3., 0., 13., 0., 11., 3., 7., 7., 3., 2., 5., 3.,
    #      15., 6., 11., 5., 8., 6.]]
    outcome_all = []
    ff = []
    for x in pop.decs:
        # 获取开始时间
        # x = [3., 0., 1., 0., 3., 1., 1., 4., 1., 4., ]
        init_time = data.get("schedule_start_time", "2023-01-19 00:00:00")
        # 统计所需人员和设备id
        available_device, available_pop = count_Need_PeopleAndAdvice(data)
        # 同一项目下的最早开始的任务时间和优先级
        experiment_id = find_EarliestTime_InSameExperiment(data, x)
        # 同一experiment_id下的task_id及项目task_sort
        same_experiment_id_task = find_Same_ExperimentId_Task(data)
        # 将每个任务按照设备分为各个组
        pop_i, experiment, time = group_TaskToAdvice(data, available_device, experiment_id, x)
        # 固定待排任务的开始和结束时间
        # try:
        time_list, pop_dict, Flag = set_SchedulingTaskTime(data, init_time, pop_i, experiment, time,
                                                           worker_index2string,
                                                           worker_string2index, same_experiment_id_task)
        # except KeyError as e:
        #     print(x)
        #     print(e)
        # 计算总时间
        f = cal_Toaltime(init_time, time_list, available_device)
        # 进行数据后处理
        outcome = []
        for h in range(time.shape[0]):
            for h1 in range(time.shape[1]):
                if ~np.isnan(experiment.loc[h][h1]):
                    tmp = {
                        "experiment_id": data["scheduling_task"][
                            int(experiment.loc[h][h1])
                        ]["experiment_id"],
                        "laboratory": data["scheduling_task"][
                            int(experiment.loc[h][h1])
                        ]["laboratory"],
                        "task_type": data["scheduling_task"][
                            int(experiment.loc[h][h1])
                        ]["task_type"],
                        "task_id": data["scheduling_task"][int(experiment.loc[h][h1])][
                            "task_id"
                        ],
                        "start_time": time_list[str(h + 1)][h1][0],
                        "end_time": time_list[str(h + 1)][h1][1],
                        "using_device": [device_index2string[str(h + 1)]],
                        "using_worker": [
                            worker_index2string[str(int(pop_i.loc[h][h1]))]
                        ],
                        "task_sort": data["scheduling_task"][int(experiment.loc[h][h1])][
                            "task_sort"
                        ],
                    }
                    outcome.append(tmp)
        ff.append(f)
        outcome_all.append(outcome)
    # 找到目标值最小的序列对应的下标
    min_index = np.argmin(np.array(ff))
    best_seq = outcome_all[min_index]

    return ff[min_index], outcome_scheduled + best_seq


if __name__ == '__main__':
    # data_set = pd.read_json("E:\\pythonProject\\platgo\\xugong-scheduling\\传动0818.json")['data']
    data_set = pd.read_json("/home/xie/xugong/xugong-scheduling/传动0825.json")["data"]
    obj_best, seq_best = main(data_set)
    with open("outcome0825.json", "w", encoding="utf-8") as file:
        json.dump(seq_best, file, ensure_ascii=False)
    print("done")

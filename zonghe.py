from queue import Queue  # noqa
import numpy as np
import pandas as pd
import datetime
import re

from components.packages.common.algo_front import (  # noqa
    algo_front,
    control_cb,
)
from components.packages.common.commons import OptOrDoE  # noqa

# from components.packages.common.engineering_mode import sim_req_cb  # noqa
from components.packages.platgo.algorithms import DE  # noqa
from components.packages.platgo.problems.single_objective.xugong.transmission import (  # noqa
    XugongTransmission,
)
from components.packages.common.install_packages import (  # noqa
    install_requirements,
)
from components.packages.common.external_optimization_problem import (  # noqa
    ext_opt_prob_cb,
)


def checkstr(item, item_key, index):
    """_summary_
    检查项目中字段值是否为str
    :param item: 待排项目
    :param item_key: 项目字段列表
    :param index: 项目下标
    :return: 报错信息
    """
    info_str = []
    for i in range(len(item_key)):
        if item_key[i] not in item:
            continue
        if type(item[item_key[i]]) != str:
            info = item_key[i] + " is not str!" + "【" + str(index) + "】"
            info_str.append(info)
        if item_key[i] in [
            "start_time",
            "end_time",
            "plan_start_time",
            "plan_end_time",
        ]:
            if not re.match(
                r"\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", item[item_key[i]]
            ):
                info = (
                    item_key[i] + " is wrong format!" + "【" + str(index) + "】"
                )
                info_str.append(info)
    return info_str


def checkint(item, item_key, index):

    info_int = []
    for i in range(len(item_key)):
        if item_key[i] not in item:
            continue
        if item_key[i] == "needed_worker":
            if type(item[item_key[i]]) != int:
                info = item_key[i] + " is not int!" + "【" + str(index) + "】"
                info_int.append(info)
            else:
                if item[item_key[i]] < 1:
                    info = (
                        item_key[i]
                        + " should be larger or equal 1!"
                        + "【"
                        + str(index)
                        + "】"
                    )
                    info_int.append(info)
        else:
            if type(item[item_key[i]]) != int:
                info = item_key[i] + " is not int!" + "【" + str(index) + "】"
                info_int.append(info)

    return info_int


def checkdict(item, item_key, index):

    info_dict = []
    for i in range(len(item_key)):
        if type(item[item_key[i]]) != dict:
            info = item_key[i] + " is not dict" + "【" + str(index) + "】"
            info_dict.append(info)
        else:
            for value in item[item_key[i]].values():
                if value < 1:
                    info = (
                        item_key[i]
                        + " should be larger or equal 1!"
                        + "【"
                        + str(index)
                        + "】"
                    )
                    info_dict.append(info)

    return info_dict


def checklist(item, item_key, index):

    info_list = []
    for i in range(len(item_key)):
        if item_key[i] not in item:
            continue
        if type(item[item_key[i]]) != list:
            info = item_key[i] + " is not list" + "【" + str(index) + "】"
            info_list.append(info)
        else:
            if len(item[item_key[i]]) > 0:
                for value in item[item_key[i]]:
                    if type(value) != str:
                        info = (
                            item_key[i]
                            + " elements should be str"
                            + "【"
                            + str(index)
                            + "】"
                        )
                        info_list.append(info)
            else:
                info = (
                    item_key[i]
                    + " should keep one element at least"
                    + "【"
                    + str(index)
                    + "】"
                )
                info_list.append(info)
    return info_list


def prechecking(data_set):
    flag = False  # 判断数据是否出错
    info = {}
    scheduling_task_info = []  # 待排项目出错信息输出
    scheduled_task_info = []  # 固定项目出错信息输出
    index1 = 0  # 记录待排项目下标
    index2 = 0  # 记录固定项目下标
    for item in data_set["scheduling_task"]:
        scheduling_info_str = checkstr(
            item,
            [
                "experiment_id",
                "laboratory",
                "plan_start_time",
                "plan_end_time",
                "task_id",
                "task_type",
            ],
            index1,
        )

        scheduling_info_int = checkint(
            item, ["task_order", "task_duration", "needed_worker"], index1
        )

        scheduling_info_dict = checkdict(
            item,
            [
                "needed_field",
                "available_field",
                "needed_equipment",
                "available_equipment",
            ],
            index1,
        )
        scheduling_info_list = checklist(
            item, ["available_worker", "using_worker"], index1
        )
        scheduling_task_info.append(scheduling_info_str)
        scheduling_task_info.append(scheduling_info_int)
        scheduling_task_info.append(scheduling_info_dict)
        scheduling_task_info.append(scheduling_info_list)

        index1 += 1
    for item in data_set["scheduled_task"]:
        scheduled_info_str = checkstr(
            item,
            [
                "experiment_id",
                "laboratory",
                "start_time",
                "end_time",
                "task_id",
                "task_type",
            ],
            index2,
        )
        scheduled_info_dict = checkdict(
            item, ["using_field", "using_equipment"], index2
        )
        scheduled_info_list = checklist(item, ["using_worker"], index2)

        scheduled_task_info.append(scheduled_info_str)
        scheduled_task_info.append(scheduled_info_dict)
        scheduled_task_info.append(scheduled_info_list)

        index2 += 1
    info["scheduling_task_info"] = sum(scheduling_task_info, [])
    info["scheduled_task_info"] = sum(scheduled_task_info, [])
    if (
        len(info["scheduling_task_info"]) != 0
        or len(info["scheduled_task_info"]) != 0
    ):
        flag = True
    return flag, info


def preprocessing(data_set):
    device_map = dict()
    workder_map = dict()
    field_map = dict()
    field_map_indexstring = dict()
    workder_map_indexstring = dict()
    field_index = 0
    worker_index = 0
    lower = []
    upper = []
    for item in data_set["scheduled_task"]:
        tmp = []
        if list(item["using_field"].keys())[0] not in field_map:
            field_map[list(item["using_field"].keys())[0]] = str(field_index)
            field_map_indexstring[str(field_index)] = list(
                item["using_field"].keys()
            )[0]
            field_index += 1
        item["using_field"][
            field_map[list(item["using_field"].keys())[0]]
        ] = item["using_field"][list(item["using_field"].keys())[0]]
        del item["using_field"][list(item["using_field"].keys())[0]]
        for worker_string in item["using_worker"]:
            if worker_string not in workder_map:
                workder_map[(worker_string)] = str(worker_index)
                workder_map_indexstring[str(worker_index)] = worker_string
                worker_index += 1
            tmp.append(workder_map[worker_string])
        item["using_worker"] = tmp
    for item in data_set["scheduling_task"]:
        if list(item["available_field"].keys())[0] not in field_map:
            field_map[list(item["available_field"].keys())[0]] = str(
                field_index
            )
            field_map_indexstring[str(field_index)] = list(
                item["available_field"].keys()
            )[0]
            field_index += 1
        item["available_field"][
            field_map[list(item["available_field"].keys())[0]]
        ] = item["available_field"][list(item["available_field"].keys())[0]]
        del item["available_field"][list(item["available_field"].keys())[0]]
        if "using_worker" in item:
            lower.append([0] * len(item["using_worker"]))
            tmp = []
            for worker_string in item["using_worker"]:
                if worker_string not in workder_map:
                    workder_map[worker_string] = str(worker_index)
                    workder_map_indexstring[str(worker_index)] = worker_string
                    worker_index += 1
                tmp.append(workder_map[worker_string])
            item["using_worker"] = tmp
        else:
            lower.append([0] * item["needed_worker"])
            tmp = []
            for worker_string in item["available_worker"]:
                if worker_string not in workder_map:
                    workder_map[worker_string] = str(worker_index)
                    workder_map_indexstring[str(worker_index)] = worker_string
                    worker_index += 1
                tmp.append(workder_map[worker_string])
            item["available_worker"] = tmp
        for i in item["available_equipment"]:
            if i not in device_map:
                device_map[i] = item["available_equipment"][i]
    lower = sum(lower, [])
    for i in range(len(lower)):
        upper.append([worker_index])
    upper = sum(upper, [])
    n_var = len(lower)
    return (
        device_map,
        field_map,
        field_map_indexstring,
        workder_map,
        workder_map_indexstring,
        lower,
        upper,
        n_var,
    )


def postprocessing(data_set, algo_result, worker_index2string):
    data = data_set
    init_time = data.get("schedule_start_time", "2023-01-19 00:00:00")
    algo_res = algo_result["data"]
    n_var = algo_result["n_var"]
    n_obj = algo_result["n_obj"]
    pop = np.array(algo_res)[1:, :n_var].astype(float).astype(int)
    objv_min = np.argmin(
        np.array(np.array(algo_res)[1:, n_var + n_obj - 1], dtype=np.float32)
    )
    time_dict = {}  # 场地时间
    pop_dict = {}  # 人员时间
    device_dict = {}  # 设备时间
    experiment_i = []
    pop_i = []  # 每个场地上人员情况
    experiment_device = {}  # 任务使用的设备
    field_nums = field_num(data)
    residue_filed = {}
    x = pop[objv_min]
    # 统计分到同一个场地的任务
    for i in range(field_nums):
        j = 0
        nums = 0  # 第几个任务
        people = []
        experiment = []
        seq = []
        while j < len(x):
            if "available_worker" in data["scheduling_task"][nums]:
                t = data["scheduling_task"][nums]["available_worker"]
                j_ = data["scheduling_task"][nums]["needed_worker"]
            else:
                t = data["scheduling_task"][nums]["using_worker"]
                j_ = len(data["scheduling_task"][nums]["using_worker"])
            if (
                int(
                    list(
                        data["scheduling_task"][nums]["available_field"].keys()
                    )[0]
                )
                == i
            ):  # 第i个场地
                residue_filed[i] = data["scheduling_task"][nums][
                    "available_field"
                ][str(i)]
                if "available_worker" in data["scheduling_task"][nums]:
                    people.append(
                        list(
                            x[
                                j : j
                                + data["scheduling_task"][nums][
                                    "needed_worker"
                                ]
                            ]
                        )
                    )
                else:
                    people.append(
                        data["scheduling_task"][nums]["using_worker"]
                    )
                if "plan_start_time" in data["scheduling_task"][nums]:
                    for h in range(j_):
                        seq.append(
                            (
                                data["scheduling_task"][nums][
                                    "plan_start_time"
                                ][0:7],
                                data["scheduling_task"][nums]["task_order"],
                            )
                        )
                        experiment.append(nums)
                else:
                    for h in range(j_):
                        seq.append(
                            (
                                "3000-13",
                                data["scheduling_task"][nums]["task_order"],
                            )
                        )
                        experiment.append(nums)
            j += j_
            nums += 1
        people = sum(people, [])
        tmp = sorted(range(len(people)), key=lambda x: (seq[x][0], seq[x][1]))
        people = np.array(people, dtype=np.float64)[tmp]
        experiment = np.array(experiment)[tmp]
        pop_i.append(people)
        experiment_i.append(experiment)
    experiment_i = pd.DataFrame(experiment_i)
    pop_i = pd.DataFrame(pop_i)
    experiment = {}  # 判断实验是否做过
    Flag_i = [0] * field_nums  # 存放场地的第几个实验
    Flag = {}  # 存放已做实验的时间
    time_dict_i = {}  # 存放每个场地中最快结束和最慢结束的时间
    #  先看每个场地的第一个实验
    while len(experiment) != len(data["scheduling_task"]):
        for j in range(experiment_i.shape[0]):
            i = Flag_i[j]
            if not np.isnan(experiment_i[i][j]):
                if (
                    len(
                        data["scheduling_task"][int(experiment_i[i][j])][
                            "pre_tasks"
                        ]
                    )
                    != 0
                ):
                    if (
                        data["scheduling_task"][int(experiment_i[i][j])][
                            "pre_tasks"
                        ][0]
                        not in Flag
                    ):
                        continue
                if i == 0:
                    # 预估的结束时间
                    t = datetime.datetime.strptime(
                        init_time, "%Y-%m-%d %H:%M:%S"
                    ) + datetime.timedelta(
                        minutes=data["scheduling_task"][
                            int(experiment_i[i][j])
                        ]["task_duration"]
                    )
                    start_time = init_time
                    if (
                        list(
                            data["scheduling_task"][int(experiment_i[i][j])][
                                "needed_field"
                            ].values()
                        )[0]
                        == residue_filed[j]
                    ):
                        num_flag = True
                    else:
                        num_flag = False
                else:
                    #  实验做过了，要么没出现在experiment中，要么experiment中为True
                    if (
                        experiment_i[i][j] in experiment
                        and experiment[int(experiment_i[i][j])]
                    ):
                        Flag_i[j] += 1
                        continue
                    if (
                        list(
                            data["scheduling_task"][int(experiment_i[i][j])][
                                "needed_field"
                            ].values()
                        )[0]
                        == residue_filed[j]
                    ):
                        num_flag = True
                        t = datetime.datetime.strptime(
                            time_dict_i[j]["end_slow"], "%Y-%m-%d %H:%M:%S"
                        ) + datetime.timedelta(
                            minutes=data["scheduling_task"][
                                int(experiment_i[i][j])
                            ]["task_duration"]
                        )
                        start_time = time_dict_i[j]["end_slow"]
                    else:  # 如果没有全部占用，则从最早结束时间开始
                        num_flag = False
                        t = datetime.datetime.strptime(
                            time_dict_i[j]["end_fast"], "%Y-%m-%d %H:%M:%S"
                        ) + datetime.timedelta(
                            minutes=data["scheduling_task"][
                                int(experiment_i[i][j])
                            ]["task_duration"]
                        )
                        start_time = time_dict_i[j]["end_fast"]
                Flag_i[j] += 1
                # 当前需要的人
                if (
                    "needed_worker"
                    in data["scheduling_task"][int(experiment_i[i][j])]
                ):
                    people1 = list(
                        pop_i.iloc[j][
                            i : i
                            + data["scheduling_task"][int(experiment_i[i][j])][
                                "needed_worker"
                            ]
                        ]
                    )
                else:
                    people1 = data["scheduling_task"][int(experiment_i[i][j])][
                        "using_worker"
                    ]
                device = list(
                    data["scheduling_task"][int(experiment_i[i][j])][
                        "needed_equipment"
                    ].keys()
                )
                device1 = []
                for h in range(len(device)):
                    device1 += [device[h].split(",")]
                device1 = cal_cartesian_coord_2(device1)  # 笛卡尔积
                time1 = []
                for h in device1:
                    #  前置实验的时间
                    if not data["scheduling_task"][int(experiment_i[i][j])][
                        "pre_tasks"
                    ]:
                        pre = []
                    else:
                        pre = Flag[
                            data["scheduling_task"][int(experiment_i[i][j])][
                                "pre_tasks"
                            ][0]
                        ]
                    start1, end1 = pan(
                        data,
                        j,
                        people1,
                        time_dict,
                        pop_dict,
                        h,
                        device_dict,
                        start_time,
                        t,
                        pre,
                        residue_filed[j],
                        num_flag,
                    )
                    time1.append([start1, str(end1)])
                # 选取最快的组合方式
                experiment_device[experiment_i[i][j]] = device1[
                    np.argmin(np.array(time1)[:, 1])
                ]
                # 更新设备表
                for h1 in list(device1[np.argmin(np.array(time1)[:, 1])]):
                    device_dict.setdefault(h1, []).append(
                        time1[np.argmin(np.array(time1)[:, 1])]
                    )
                # 更新人员表
                for h in people1:
                    pop_dict.setdefault(h, []).append(
                        time1[np.argmin(np.array(time1)[:, 1])]
                    )
                # 更新场地表
                time_dict.setdefault(j, []).append(
                    time1[np.argmin(np.array(time1)[:, 1])]
                )
                experiment[int(experiment_i[i][j])] = True
                Flag[
                    data["scheduling_task"][int(experiment_i[i][j])]["task_id"]
                ] = time1[np.argmin(np.array(time1)[:, 1])]
                if len(time_dict[j]) < int(
                    residue_filed[j]
                ):  # 当场地上任务数小于容量时，开始时间就是初始时间。
                    end_fast = init_time
                else:
                    end_fast = np.array(time_dict[j])[
                        np.argmin(np.array(time_dict[j])[:, 1])
                    ][-1]
                end_slow = np.array(time_dict[j])[
                    np.argmax(np.array(time_dict[j])[:, 1])
                ][-1]
                time_dict_i[j] = {"end_fast": end_fast, "end_slow": end_slow}
    outcome = []
    Flag = {}
    Flag_i = [0] * field_nums
    for i in range(experiment_i.shape[0]):
        for j in range(experiment_i.shape[1]):
            if not np.isnan(experiment_i.loc[i][j]):
                if experiment_i.loc[i][j] not in Flag:
                    Flag[experiment_i.loc[i][j]] = True
                    using_equipment = {}
                    for item in list(
                        experiment_device[experiment_i.loc[i][j]]
                    ):
                        using_equipment[item] = data["scheduling_task"][
                            int(experiment_i.loc[i][j])
                        ]["available_equipment"][item]
                    if (
                        "needed_worker"
                        in data["scheduling_task"][int(experiment_i.loc[i][j])]
                    ):
                        t = {
                            "experiment_id": data["scheduling_task"][
                                int(experiment_i.loc[i][j])
                            ]["experiment_id"],
                            "laboratory": data["scheduling_task"][
                                int(experiment_i.loc[i][j])
                            ]["laboratory"],
                            "task_id": data["scheduling_task"][
                                int(experiment_i.loc[i][j])
                            ]["task_id"],
                            "start_time": time_dict[i][Flag_i[i]][0],
                            "end_time": time_dict[i][Flag_i[i]][1],
                            "pre_tasks": data["scheduling_task"][
                                int(experiment_i.loc[i][j])
                            ]["pre_tasks"],
                            "task_type": data["scheduling_task"][
                                int(experiment_i.loc[i][j])
                            ]["task_type"],
                            "using_worker": [
                                worker_index2string[str(int(h))]
                                for h in list(
                                    pop_i.iloc[i][
                                        j : j
                                        + data["scheduling_task"][
                                            int(experiment_i.loc[i][j])
                                        ]["needed_worker"]
                                    ]
                                )
                            ],
                            "using_field": data["scheduling_task"][
                                int(experiment_i.loc[i][j])
                            ]["needed_field"],
                            "using_equipment": using_equipment,
                        }
                        Flag_i[i] += 1
                    else:
                        t = {
                            "experiment_id": data["scheduling_task"][
                                int(experiment_i.loc[i][j])
                            ]["experiment_id"],
                            "laboratory": data["scheduling_task"][
                                int(experiment_i.loc[i][j])
                            ]["laboratory"],
                            "task_id": data["scheduling_task"][
                                int(experiment_i.loc[i][j])
                            ]["task_id"],
                            "start_time": time_dict[i][Flag_i[i]][0],
                            "end_time": time_dict[i][Flag_i[i]][1],
                            "pre_tasks": data["scheduling_task"][
                                int(experiment_i.loc[i][j])
                            ]["pre_tasks"],
                            "task_type": data["scheduling_task"][
                                int(experiment_i.loc[i][j])
                            ]["task_type"],
                            "using_worker": [
                                worker_index2string[str(int(h))]
                                for h in data["scheduling_task"][
                                    int(experiment_i.loc[i][j])
                                ]["using_worker"]
                            ],
                            "using_field": data["scheduling_task"][
                                int(experiment_i.loc[i][j])
                            ]["needed_field"],
                            "using_equipment": using_equipment,
                        }
                        Flag_i[i] += 1
                    outcome.append(t)
    return outcome


def pan(
    data,
    field,
    worker,
    time_dict,
    pop_dict,
    device,
    device_dict,
    start,
    end,
    pre,
    residue_filed,
    num_flag,
):  # noqa
    time = []
    if pre:
        time.append(pre)
    duration = end - datetime.datetime.strptime(start, "%Y-%m-%d %H:%M:%S")
    for j in range(len(data["scheduled_task"])):
        if (
            int(list(data["scheduled_task"][j]["using_field"].keys())[0])
            == field
            and list(data["scheduled_task"][j]["using_field"].values())[0]
            == residue_filed
        ):
            time.append(
                [
                    data["scheduled_task"][j]["start_time"],
                    data["scheduled_task"][j]["end_time"],
                ]
            )
    for i in worker:
        for j in range(len(data["scheduled_task"])):
            if str(int(float(i))) in data["scheduled_task"][j]["using_worker"]:
                time.append(
                    [
                        data["scheduled_task"][j]["start_time"],
                        data["scheduled_task"][j]["end_time"],
                    ]
                )
        if i in pop_dict:
            time.append(pop_dict[i][-1])
    for i in device:
        for j in range(len(data["scheduled_task"])):
            if i in data["scheduled_task"][j]["using_equipment"]:
                time.append(
                    [
                        data["scheduled_task"][j]["start_time"],
                        data["scheduled_task"][j]["end_time"],
                    ]
                )
        if i in device_dict:
            for kk in range(len(device_dict[i])):
                time.append(device_dict[i][kk])
    time.sort(key=lambda x: (x[0], x[1]))
    Flag = True
    while Flag:
        flag = True
        start, end = judege(start, end, duration)
        for i in range(len(time)):
            if (time[i][0] < start < time[i][1]) or (
                time[i][0] < str(end) < time[i][1]
            ):
                start = time[i][1]
            elif str(end) <= time[i][0]:
                flag = False
            elif start <= time[i][0] and str(end) >= time[i][1]:
                start = time[i][1]
            start, end = judege(start, end, duration)
            if not flag:
                break
        time1 = [[start, str(end)]]
        if not num_flag:  # 没有全部占用的时候，需要考虑同场地，全部占用的时候，已经避开了场地其他
            # 任务，只需要跟固定任务比即可
            if field in time_dict:
                time1 += time_dict[field]
        time1.sort(key=lambda x: (x[0], x[1]))
        for j in range(len(data["scheduled_task"])):
            if (
                int(list(data["scheduled_task"][j]["using_field"].keys())[0])
                == field
            ):
                if (
                    int(
                        list(
                            data["scheduled_task"][j]["using_field"].values()
                        )[0]
                    )
                    != 1
                ):
                    continue
                #  t是固定任务的时间
                t = [
                    data["scheduled_task"][j]["start_time"],
                    data["scheduled_task"][j]["end_time"],
                ]
                if not attack(
                    time1,
                    t,
                    int(
                        list(
                            data["scheduled_task"][j]["using_field"].values()
                        )[0]
                    ),
                    residue_filed,
                ):
                    start = t[1]
                    end = (
                        datetime.datetime.strptime(start, "%Y-%m-%d %H:%M:%S")
                        + duration
                    )
                    break
        else:
            Flag = False
    return start, end


def judege(start, end, duration):
    if "02-16" < start[5:10] < "07-14":
        if (
            "08:30:00" <= start[11:] < "12:00:00"
            or "13:30:00" <= start[11:] < "18:00:00"
        ):
            start = start
        elif "12:00:00" <= start[11:] < "13:30:00":
            start = start[:11] + "13:30:00"
        elif "00:00:00" <= start[11:] < "08:30:00":
            start = start[:11] + "08:30:00"
        else:
            start = (
                str(
                    datetime.datetime.strptime(start[:10], "%Y-%m-%d")
                    + datetime.timedelta(days=1)
                )[:11]
                + "08:30:00"
            )
    elif "07-15" < start[5:10] < "09-30":
        if (
            "08:30:00" <= start[11:] < "12:00:00"
            or "14:00:00" <= start[11:] < "18:00:00"
        ):
            start = start
        elif "12:00:00" <= start[11:] < "14:00:00":
            start = start[:11] + "14:00:00"
        elif "00:00:00" <= start[11:] < "08:30:00":
            start = start[:11] + "08:30:00"
        else:
            start = (
                str(
                    datetime.datetime.strptime(start[:10], "%Y-%m-%d")
                    + datetime.timedelta(days=1)
                )[:11]
                + "08:30:00"
            )
    elif "10-01" < start[5:10] < "11-15":
        if (
            "08:30:00" <= start[11:] < "12:00:00"
            or "13:30:00" <= start[11:] < "18:00:00"
        ):
            start = start
        elif "12:00:00" <= start[11:] < "13:30:00":
            start = start[:11] + "13:30:00"
        elif "00:00:00" <= start[11:] < "08:30:00":
            start = start[:11] + "08:30:00"
        else:
            start = (
                str(
                    datetime.datetime.strptime(start[:10], "%Y-%m-%d")
                    + datetime.timedelta(days=1)
                )[:11]
                + "08:30:00"
            )
    else:
        if (
            "08:30:00" <= start[11:] < "12:00:00"
            or "13:30:00" <= start[11:] < "17:30:00"
        ):
            start = start
        elif "12:00:00" <= start[11:] < "13:30:00":
            start = start[:11] + "13:30:00"
        elif "00:00:00" <= start[11:] < "08:30:00":
            start = start[:11] + "08:30:00"
        else:
            start = (
                str(
                    datetime.datetime.strptime(start[:10], "%Y-%m-%d")
                    + datetime.timedelta(days=1)
                )[:11]
                + "08:30:00"
            )
    end = datetime.datetime.strptime(start, "%Y-%m-%d %H:%M:%S") + duration
    return start, end


def attack(time, t, nums, residue_filed):
    s = 0
    for i in range(len(time)):
        if (
            (time[i][0] < t[0] < time[i][1])
            or (time[i][0] < t[1] < time[i][1])
            or (t[0] <= time[i][0] and t[1] >= time[i][1])
        ):
            s += 1
    if s + nums <= residue_filed:
        return True
    else:
        return False


def field_num(data_set):
    field = 0
    field_map = []
    for item in data_set["scheduling_task"]:
        if list(item["available_field"].keys())[0] not in field_map:
            field_map.append(list(item["available_field"].keys())[0])
            field += 1
    for item in data_set["scheduled_task"]:
        if list(item["using_field"].keys())[0] not in field_map:
            field_map.append(list(item["using_field"].keys())[0])
            field += 1
    return field


def cal_cartesian_coord_2(values):
    mesh = np.meshgrid(*values)
    cart = np.array(mesh).T.reshape(-1, len(values))
    return cart.tolist()


def search_sqp(data_set):

    flag, info = prechecking(data_set)
    if flag:
        return info
    else:
        (
            device_map,
            field_map,
            field_map_indexstring,
            workder_map,
            workder_map_indexstring,
            lower,
            upper,
            n_var,
        ) = preprocessing(data_set)

        optimization_problem = {
            "name": "XugongComprehensive",
            "n_var": n_var,
            "lower": lower,
            "upper": upper,
            "dataSet": [data_set],
            "algoResultType": 0,
        }

        pop_size = 20
        max_fe = 100
        options = {}
        simulation_request_callback = None

        def run_algo():
            evol_algo = DE(
                pop_size=pop_size,
                options=options,
                optimization_problem=optimization_problem,
                simulation_request_callback=simulation_request_callback,  # noqa
                max_fe=max_fe,
                name="DE-Thread",
                debug=True,
            )
            evol_algo.start()
            evol_algo.join()
            algo_result = evol_algo.get_external_algo_result()

            schedule_result = postprocessing(
                data_set, algo_result, workder_map_indexstring
            )
            print(schedule_result)
            print("Done")
            return schedule_result

        return run_algo()


if __name__ == "__main__":
    data_set = pd.read_json("/home/xie/xugong/xugong-scheduling/试验场排程.json")[
        "data"
    ]
    final_result = search_sqp(data_set)
    print(final_result)
    print("done")

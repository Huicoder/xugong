from queue import Queue  # noqa
import random
import numpy as np
import pandas as pd
import datetime
import re

from components.packages.common.algo_front import (  # noqa
    algo_front,
    control_cb,
)
from components.packages.common.commons import OptOrDoE  # noqa

# from components.packages.common.engineering_mode import sim_req_cb  # noqa
from components.packages.platgo.algorithms import DE  # noqa
from components.packages.platgo.problems.single_objective.xugong.transmission import (  # noqa
    XugongTransmission,
)
from components.packages.common.install_packages import (  # noqa
    install_requirements,
)
from components.packages.common.external_optimization_problem import (  # noqa
    ext_opt_prob_cb,
)


def checkstr(item, item_key, index):
    """_summary_
    检查项目中字段值是否为str
    :param item: 待排项目
    :param item_key: 项目字段列表
    :param index: 项目下标
    :return: 报错信息
    """
    info_str = []
    for i in range(len(item_key)):
        if item_key[i] not in item:
            continue
        if type(item[item_key[i]]) != str:
            info = item_key[i] + " is not str!" + "【" + str(index) + "】"
            info_str.append(info)
        if item_key[i] in [
            "start_time",
            "end_time",
            "plan_start_time",
            "plan_end_time",
        ]:
            if not re.match(
                r"\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", item[item_key[i]]
            ):
                info = (
                    item_key[i] + " is wrong format!" + "【" + str(index) + "】"
                )
                info_str.append(info)
    return info_str


def checkint(item, item_key, index):

    info_int = []
    for i in range(len(item_key)):
        if item_key[i] == "task_sort":
            if type(item[item_key[i]]) != int:
                info = item_key[i] + " is not int!" + "【" + str(index) + "】"
                info_int.append(info)
            else:
                if item[item_key[i]] < 1:
                    info = (
                        item_key[i]
                        + " should be larger or equal 1!"
                        + "【"
                        + str(index)
                        + "】"
                    )
                    info_int.append(info)
        else:
            if type(item[item_key[i]]) != int:
                info = item_key[i] + " is not int!" + "【" + str(index) + "】"
                info_int.append(info)

    return info_int


def checkdict(item, item_key, index):

    info_dict = []
    for i in range(len(item_key)):
        if type(item[item_key[i]]) != dict:
            info = item_key[i] + " is not dict" + "【" + str(index) + "】"
            info_dict.append(info)
        else:
            for value in item[item_key[i]].values():
                if value < 1:
                    info = (
                        item_key[i]
                        + " should be larger or equal 1!"
                        + "【"
                        + str(index)
                        + "】"
                    )
                    info_dict.append(info)

    return info_dict


def prechecking(data_set):
    flag = False  # 判断数据是否出错
    info = {}
    scheduling_task_info = []  # 待排项目出错信息输出
    scheduled_task_info = []  # 固定项目出错信息输出
    index1 = 0  # 记录待排项目下标
    index2 = 0  # 记录固定项目下标
    for item in data_set["scheduling_task"]:
        scheduling_info_str = checkstr(
            item,
            [
                "experiment_id",
                "laboratory",
                "plan_start_time",
                "plan_end_time",
                "task_id",
                "task_type",
            ],
            index1,
        )

        scheduling_info_int = checkint(
            item, ["task_order", "task_sort", "task_duration"], index1
        )

        scheduling_info_dict = checkdict(
            item, ["available_device", "available_worker"], index1
        )
        scheduling_task_info.append(scheduling_info_str)
        scheduling_task_info.append(scheduling_info_int)
        scheduling_task_info.append(scheduling_info_dict)
        index1 += 1
    for item in data_set["scheduled_task"]:
        scheduled_info_str = checkstr(
            item,
            [
                "experiment_id",
                "laboratory",
                "start_time",
                "end_time",
                "task_id",
                "task_type",
            ],
            index2,
        )
        scheduled_info_int = checkint(item, ["task_sort"], index2)
        scheduled_info_dict = checkdict(
            item, ["using_device", "using_worker"], index2
        )
        scheduled_task_info.append(scheduled_info_str)
        scheduled_task_info.append(scheduled_info_int)
        scheduled_task_info.append(scheduled_info_dict)

        index2 += 1
    info["scheduling_task_info"] = sum(scheduling_task_info, [])
    info["scheduled_task_info"] = sum(scheduled_task_info, [])
    if (
        len(info["scheduling_task_info"]) != 0
        or len(info["scheduled_task_info"]) != 0
    ):
        flag = True
    return flag, info


def preprocessing(data_set):
    device_string2index = dict()
    worker_string2index = dict()
    device_index2string = dict()
    worker_index2string = dict()
    device_index = 1
    worker_index = 0
    tasks = len(data_set["scheduling_task"])

    for item in data_set["scheduling_task"]:
        for i, device_string in enumerate(item["available_device"]):
            if device_string not in device_string2index:
                device_string2index[device_string] = []
                device_index2string[str(device_index)] = []

                device_value = item["available_device"][device_string]
                for k in range(device_index, device_index + device_value):
                    device_string2index[device_string] += [k]
                    device_index2string[str(k)] = device_string
                    device_index += 1
            item["available_device"][device_string] = [
                str(i) for i in device_string2index[device_string]
            ]
        item["available_device"] = sum(
            list(map(lambda x: x, item["available_device"].values())), []
        )
        for i, worker_string in enumerate(item["available_worker"]):
            if worker_string not in worker_string2index:
                worker_string2index[worker_string] = []
                worker_index2string[str(worker_index)] = []

                worker_value = item["available_worker"][worker_string]
                for k in range(worker_index, worker_index + worker_value):
                    worker_string2index[worker_string] += [k]
                    worker_index2string[str(k)] = worker_string
                    worker_index += 1
            item["available_worker"][worker_string] = [
                str(i) for i in worker_string2index[worker_string]
            ]
        # 使用lambda函数结合map()方法获取字典中的所有值,并进行列表合并

        item["available_worker"] = sum(
            list(map(lambda x: x, item["available_worker"].values())), []
        )

    for item in data_set["scheduled_task"]:
        for i, device_string in enumerate(item["using_device"]):
            if device_string not in device_string2index:
                device_string2index[device_string] = []
                device_index2string[str(device_index)] = []
                device_value = item["using_device"][device_string]
                for k in range(device_index, device_index + device_value):
                    device_string2index[device_string] += [k]
                    device_index2string[str(k)] = device_string
                    device_index += 1

            item["using_device"] = [
                str(int(random.choice(device_string2index[device_string])))
            ]

        for i, worker_string in enumerate(item["using_worker"]):
            if worker_string not in worker_string2index:
                worker_string2index[worker_string] = []
                worker_index2string[str(worker_index)] = []
                worker_value = item["using_worker"][worker_string]
                for k in range(worker_index, worker_index + worker_value):
                    worker_string2index[worker_string] += [k]
                    worker_index2string[str(k)] = worker_string
                    worker_index += 1
            item["using_worker"] = [
                str(int(random.choice(worker_string2index[worker_string])))
            ]

    return (
        device_index2string,
        worker_index2string,
        worker_string2index,
        device_index,
        worker_index,
        tasks,
    )


def postprocessing(
    data_set,
    algo_result,
    device_index2string,
    worker_index2string,
    worker_string2index,
):
    data = data_set
    init_time = data.get("schedule_start_time", "2023-01-19 00:00:00")
    algo_res = algo_result["data"]
    n_var = algo_result["n_var"]
    n_obj = algo_result["n_obj"]
    pop = np.array(algo_res)[1:, :n_var].astype(float).astype(int)
    # 最后一列是约束，倒数第二列是目标值
    objv_min = np.argmin(
        np.array(np.array(algo_res)[1:, n_var + n_obj - 1], dtype=np.float32)
    )
    available_device = []
    available_pop = []
    i = max(len(data["scheduling_task"]), len(data["scheduled_task"]))
    for k in range(i):
        if k < len(data["scheduling_task"]):
            available_device.append(
                data["scheduling_task"][k]["available_device"]
            )
            available_pop.append(
                data["scheduling_task"][k]["available_worker"]
            )
        if k < len(data["scheduled_task"]):
            available_pop.append(data["scheduled_task"][k]["using_worker"])
            available_device.append(data["scheduled_task"][k]["using_device"])
    available_device = list(set(sum(available_device, [])))
    available_pop = list(set(sum(available_pop, [])))
    D = len(pop[0])

    experiment_id = {}
    if len(data["scheduling_task"]) != 0:
        if data["scheduling_task"][0]["laboratory"] == "CD":
            for j in range(0, D, 2):
                if "plan_start_time" in data["scheduling_task"][int(j / 2)]:
                    tt = data["scheduling_task"][int(j / 2)]["experiment_id"]
                    if (
                        data["scheduling_task"][int(j / 2)]["experiment_id"]
                        not in experiment_id
                    ):
                        experiment_id.setdefault(tt, []).append(
                            (
                                data["scheduling_task"][int(j / 2)][
                                    "plan_start_time"
                                ][0:7],
                                data["scheduling_task"][int(j / 2)][
                                    "task_order"
                                ],
                            )
                        )
                    else:
                        if (
                            data["scheduling_task"][int(j / 2)][
                                "plan_start_time"
                            ][0:7]
                            < experiment_id[
                                data["scheduling_task"][int(j / 2)][
                                    "experiment_id"
                                ]
                            ][0][0]
                        ):
                            tt = data["scheduling_task"][int(j / 2)][
                                "experiment_id"
                            ]
                            experiment_id[tt] = [
                                (
                                    data["scheduling_task"][int(j / 2)][
                                        "plan_start_time"
                                    ][0:7],
                                    data["scheduling_task"][int(j / 2)][
                                        "task_order"
                                    ],
                                )
                            ]
                else:
                    tt = data["scheduling_task"][int(j / 2)]["experiment_id"]
                    if (
                        data["scheduling_task"][int(j / 2)]["experiment_id"]
                        not in experiment_id
                    ):
                        experiment_id.setdefault(tt, []).append(
                            (
                                "3000-13",
                                data["scheduling_task"][int(j / 2)][
                                    "task_order"
                                ],
                            )
                        )

    pop_i = []  # 对应实验台上实验人的情况
    time = []
    experiment = []
    for k in range(1, len(available_device) + 1):
        experiment_time = []
        people = []
        order = []
        seq1 = []
        t = []
        for j in range(0, D, 2):
            if pop[objv_min][j] == k:
                people.append(pop[objv_min][j + 1])
                experiment_time.append(
                    data["scheduling_task"][int(j / 2)]["task_duration"]
                )
                tt = data["scheduling_task"][int(j / 2)]["experiment_id"]
                if (
                    "plan_start_time" in data["scheduling_task"][int(j / 2)]
                ):  # 有计划开始时间的待排任务
                    if (
                        data["scheduling_task"][int(j / 2)]["laboratory"]
                        == "CD"
                    ):  # 该待排任务属于传动实验室
                        # experiment_id[tt]->[('2023-03', 5)]
                        seq1.append(
                            (
                                data["scheduling_task"][int(j / 2)][
                                    "task_sort"
                                ],
                                experiment_id[tt][0][0],
                                experiment_id[tt][0][1],
                                tt,
                            )
                        )
                    else:  # 该待排任务属于普通实验室
                        seq1.append(
                            (
                                data["scheduling_task"][int(j / 2)][
                                    "task_sort"
                                ],
                                data["scheduling_task"][int(j / 2)][
                                    "plan_start_time"
                                ][
                                    0:7
                                ],  # noqa
                                data["scheduling_task"][int(j / 2)][
                                    "task_order"
                                ],
                                tt,
                            )
                        )  # noqa
                else:  # 没有计划开始时间的待排任务
                    if (
                        data["scheduling_task"][int(j / 2)]["laboratory"]
                        == "CD"
                    ):  # 该待排任务属于传动实验室
                        seq1.append(
                            (
                                data["scheduling_task"][int(j / 2)][
                                    "task_sort"
                                ],
                                "3000-13",
                                experiment_id[tt][0][1],
                                tt,
                            )
                        )
                    else:  # 该待排任务属于普通实验室
                        seq1.append(
                            (
                                data["scheduling_task"][int(j / 2)][
                                    "task_sort"
                                ],
                                "3000-13",
                                data["scheduling_task"][int(j / 2)][
                                    "task_order"
                                ],
                                tt,
                            )
                        )
                order.append(data["scheduling_task"][int(j / 2)]["task_order"])
                t.append(int(j / 2))
        # 根据元组第0个值升序排序，若第0个值相等则根据第1个值升序排序
        # 先按照年月份排序再按照优先级排序
        tmp1 = sorted(
            range(len(t)), key=lambda x: (seq1[x][0], seq1[x][1], seq1[x][2])
        )
        pop_i.append(np.array(people)[tmp1])
        experiment.append(np.array(t)[tmp1])
        time.append(np.array(experiment_time)[tmp1])
    pop_i = pd.DataFrame(pop_i)
    experiment = pd.DataFrame(experiment)
    time = pd.DataFrame(time)

    time_list = (
        {}
    )  # 记录每个设备的占用时间段{'1': [['2023-03-30 10:20:00', '2023-03-30 11:10:00']]}
    pop_dict = (
        {}
    )  # 记录每个人员的占用时间段{'2': [['2023-03-30 10:20:00', '2023-03-30 11:10:00']]}
    experiment_done = {}  # 判断实验是否做过
    Flag = {}  # 存放已做实验的时间(key值为task_id)
    Flag_c = [0] * experiment.shape[0]  # 存放设备的第几个实验
    using_worker = (
        []
    )  # 固定任务所需人员 [list(['0']) list(['1']) list(['2'])]->['0' '1' '2']
    using_device = (
        []
    )  # 固定任务所需设备 [list(['1']) list(['2']) list(['1'])]->['1' '2' '1']

    using_worker = []
    using_device = []
    if len(data["scheduled_task"]):
        using_worker = np.array(
            pd.DataFrame(data["scheduled_task"])["using_worker"]
        )
        using_device = np.array(
            pd.DataFrame(data["scheduled_task"])["using_device"]
        )
    for k in range(len(using_device)):
        using_worker[k] = using_worker[k][0]
        using_device[k] = using_device[k][0]

    # 记录同一experiment_id下的待排任务的task_id和task_sort
    same_experiment_id_task = {}
    for item in data["scheduling_task"]:
        same_experiment_id_task.setdefault(item["experiment_id"], []).append(
            (item["task_id"], item["task_sort"])
        )

    # experiment表的最后添加一列空值
    last_col = experiment.shape[1]
    experiment[last_col] = np.nan

    while len(experiment_done) != len(data["scheduling_task"]):  # 确保所有的实验全部做完
        for j in range(experiment.shape[0]):  # 对每个设备进行遍历
            c = Flag_c[j]  # 存放当前设备的第几个实验
            if ~np.isnan(experiment[c][j]):
                task_sort = data["scheduling_task"][int(experiment[c][j])][
                    "task_sort"
                ]  # 待排任务的顺序
                task_list = []
                if task_sort > 1:  # 待排任务顺序靠后
                    task_flag = True  # 判断任务顺序合规
                    for item in same_experiment_id_task[
                        data["scheduling_task"][int(experiment[c][j])][
                            "experiment_id"
                        ]
                    ]:
                        if item[1] < task_sort:  # 找到顺序在前的项目的id
                            task_list.append(item[0])
                    for tid in task_list:
                        if tid not in Flag:  # 顺序在前的项目没有做
                            task_flag = False  # 不合规
                    if not task_flag:  # 不合规跳过该项目
                        continue
                if c == 0:  # 先看每个设备上的第一个项目
                    if (
                        str(int(pop_i[c][j])) not in pop_dict
                    ):  # 该待排任务的所需人员不在pop_dict中
                        init_time1 = init_time
                    else:
                        if (
                            data["scheduling_task"][int(experiment[c][j])][
                                "task_type"
                            ]
                            == "性能"
                        ):
                            init_time1 = init_time
                            for i in worker_string2index[
                                worker_index2string[str(int(pop_i[c][j]))]
                            ]:
                                if str(i) in pop_dict:
                                    if pop_dict[str(i)][-1][1] > init_time1:
                                        init_time1 = pop_dict[str(i)][-1][1]
                        else:
                            init_time1 = pop_dict[str(int(pop_i[c][j]))][-1][1]
                    pre = []  # 顺序在前待排任务的开始和结束时间
                    if len(task_list) != 0:
                        for tid in task_list:
                            pre.append(Flag[tid])
                        pre.sort(key=lambda x: (x[0], x[1]))
                        init_time1 = max(pre[-1][1], init_time1)
                    time_tmp = datetime.datetime.strptime(
                        init_time1, "%Y-%m-%d %H:%M:%S"
                    ) + datetime.timedelta(
                        minutes=data["scheduling_task"][int(experiment[c][j])][
                            "task_duration"
                        ]
                    )
                else:  # 再看场地的第二个及以后的实验
                    if str(int(pop_i[c][j])) not in pop_dict:
                        init_time1 = time_list[str(j + 1)][-1][1]
                    else:
                        if (
                            data["scheduling_task"][int(experiment[c][j])][
                                "task_type"
                            ]
                            == "性能"
                        ):
                            init_time1 = init_time
                            for i in worker_string2index[
                                worker_index2string[str(int(pop_i[c][j]))]
                            ]:
                                if str(i) in pop_dict:
                                    if pop_dict[str(i)][-1][1] > init_time1:
                                        init_time1 = pop_dict[str(i)][-1][1]
                        else:
                            init_time1 = pop_dict[str(int(pop_i[c][j]))][-1][1]
                        init_time1 = max(
                            time_list[str(j + 1)][-1][1], init_time1,
                        )
                        pre = []  # 顺序在前待排任务的开始和结束时间
                        if len(task_list) != 0:
                            for tid in task_list:
                                pre.append(Flag[tid])
                            pre.sort(key=lambda x: (x[0], x[1]))
                            init_time1 = max(pre[-1][1], init_time1)
                    time_tmp = datetime.datetime.strptime(
                        init_time1, "%Y-%m-%d %H:%M:%S"
                    ) + datetime.timedelta(
                        minutes=data["scheduling_task"][int(experiment[c][j])][
                            "task_duration"
                        ]
                    )
                Flag_c[j] += 1  # 第j个设备的项目加1

                init_time1, time_tmp = pan(
                    j + 1,
                    pop_i[c][j],
                    data,
                    using_worker,
                    using_device,
                    init_time1,
                    time_tmp,
                    data["scheduling_task"][int(experiment[c][j])][
                        "task_duration"
                    ],
                    data["scheduling_task"][int(experiment[c][j])][
                        "task_type"
                    ],
                    time_list,
                    pop_dict,
                    worker_string2index,
                    worker_index2string,
                    pre,
                )
                # 更新设备占用表
                time_list.setdefault(str(int(j + 1)), []).append(
                    [init_time1, str(time_tmp)]
                )
                # 更新人员占用表
                if (
                    data["scheduling_task"][int(experiment[c][j])]["task_type"]
                    == "性能"
                ):
                    for i in worker_string2index[
                        worker_index2string[str(int(pop_i[c][j]))]
                    ]:
                        pop_dict.setdefault(str(i), []).append(
                            [init_time1, str(time_tmp)]
                        )
                else:
                    pop_dict.setdefault(str(int(pop_i[c][j])), []).append(
                        [init_time1, str(time_tmp)]
                    )
                # 记录已做的实验
                experiment_done[str(int(experiment[c][j]))] = True
                Flag[
                    data["scheduling_task"][int(experiment[c][j])]["task_id"]
                ] = [init_time1, str(time_tmp)]
    outcome = []
    for h in range(time.shape[0]):
        for h1 in range(time.shape[1]):
            if ~np.isnan(experiment.loc[h][h1]):
                tmp = {
                    "experiment_id": data["scheduling_task"][
                        int(experiment.loc[h][h1])
                    ]["experiment_id"],
                    "laboratory": data["scheduling_task"][
                        int(experiment.loc[h][h1])
                    ]["laboratory"],
                    "task_type": data["scheduling_task"][
                        int(experiment.loc[h][h1])
                    ]["task_type"],
                    "task_id": data["scheduling_task"][
                        int(experiment.loc[h][h1])
                    ]["task_id"],
                    "start_time": time_list[str(h + 1)][h1][0],
                    "end_time": time_list[str(h + 1)][h1][1],
                    "using_device": [device_index2string[str(h + 1)]],
                    "using_worker": [
                        worker_index2string[str(int(pop_i.loc[h][h1]))]
                    ],
                    "task_sort": data["scheduling_task"][
                        int(experiment.loc[h][h1])
                    ]["task_sort"],
                }
                outcome.append(tmp)
    # outcome = test(outcome, data)
    return outcome


def pan(
    device,
    worker,
    data,
    using_worker,
    using_device,
    init_time,
    time_tmp,
    time1,
    task_type,
    time_list,
    pop_dict,
    worker_string2index,
    worker_index2string,
    pre,
):
    """
    判断待排任务与固定任务是否冲突，时间冲突则进行调整
    :param device: 待排任务占用的设备id
    :param worker: 待排任务占用的人员id
    :param data: 数据集
    :param using_worker: 每个固定任务所需人员id
    :param using_device: 每个固定任务所需设备id
    :param init_time: 数据集中给出的开始时间
    :param time_tmp: 该待排任务的结束时间
    :param time1: 该待排任务的实验时长
    :param task_type: 实验类型
    :param time_list: # 每个设备的使用时间段
    :param pop_dict:  # 每个人员的占用时间段
    :param worker_string2index
    :param worker_index2string
    :param pre
    :return: 该待排任务的在解决冲突后的开始时间和结束时间
    """
    time = []
    if pre:  # 当前待排任务顺序靠后
        time = pre  # 将顺序在前的任务的时间加入
    # 在固定任务中找出与当前待排任务相同的人员的开始时间和结束时间
    # 其实也在固定任务中找出当前待排任务所需人员的所有的工作时间段
    if task_type == "可靠性":
        for i in np.where(using_worker == str(int(worker)))[0]:
            time.append(
                [
                    data["scheduled_task"][i]["start_time"],
                    data["scheduled_task"][i]["end_time"],
                ]
            )
    else:
        for j in worker_string2index[worker_index2string[str(int(worker))]]:
            for i in np.where(using_worker == str(int(j)))[0]:
                time.append(
                    [
                        data["scheduled_task"][i]["start_time"],
                        data["scheduled_task"][i]["end_time"],
                    ]
                )

    # 在固定任务中找出与当前待排任务相同的设备的开始时间和结束时间
    # 其实也在固定任务中找出当前待排任务所需设备的所有工作时间段
    for i in np.where(using_device == str(int(device)))[0]:
        time.append(
            [
                data["scheduled_task"][i]["start_time"],
                data["scheduled_task"][i]["end_time"],
            ]
        )
    if str(int(device)) in time_list:
        for i in time_list[str(int(device))]:
            time.append(i)
    if str(int(worker)) in pop_dict:
        for i in pop_dict[str(int(worker))]:
            time.append(i)
    time.sort(key=lambda x: (x[0], x[1]))
    flag = True  # 判断待排任务是否固定 True为不固定
    # 根据时间表调整当前待排任务的开始时间和结束时间
    init_time, time_tmp = juddge(init_time, time_tmp, time1)
    for i in range(len(time)):
        # 判断当前的待排任务与固定任务的时间是否冲突
        if (
            time[i][0] < init_time < time[i][1]
            or time[i][0] < str(time_tmp) < time[i][1]
        ):
            init_time = time[i][1]  # 时间冲突，将待排任务的开始时间设置发生冲突固定任务的结束时间
        elif init_time <= time[i][0] and str(time_tmp) >= time[i][1]:
            init_time = time[i][1]  # 时间冲突，将待排任务的开始时间设置发生冲突固定任务的结束时间
        elif (
            str(time_tmp) <= time[i][0]
        ):  # 待排任务的结束时间在固定任务开始时间之前，此时待排任务的位置就可以固定了
            flag = False  # 待排任务和固定任务没有发生时间冲突
        # 解决时间冲突后，计算当前待排任务的实际的开始时间和结束时间
        init_time, time_tmp = juddge(init_time, time_tmp, time1)
        if not flag:  # flag为False说明待排任务已经解决与固定任务的时间冲突，且位置已经固定
            break
    return init_time, time_tmp


def juddge(init_time, time_tmp, time1):
    """
    根据时间表调整待排任务的开始时间和结束时间
    :param init_time: 待排任务的开始时间
    :param time_tmp: 待排任务的结束时间
    :param time1:  待排任务的实验时间
    :return:  调整后的开始时间和结束时间
    """
    if "02-16" <= init_time[5:10] <= "07-14":
        if (
            "08:30:00" <= init_time[11:] < "12:00:00"
            or "13:30:00" <= init_time[11:] < "18:00:00"
        ):
            init_time = init_time
        elif "12:00:00" <= init_time[11:] < "13:30:00":
            init_time = init_time[:11] + "13:30:00"
        elif "00:00:00" <= init_time[11:] < "08:30:00":
            init_time = init_time[:11] + "08:30:00"
        else:
            init_time = (
                str(
                    datetime.datetime.strptime(init_time[:10], "%Y-%m-%d")
                    + datetime.timedelta(days=1)
                )[:11]
                + "08:30:00"
            )
    elif "07-15" <= init_time[5:10] <= "09-30":
        if (
            "08:30:00" <= init_time[11:] < "12:00:00"
            or "14:00:00" <= init_time[11:] < "18:00:00"
        ):
            init_time = init_time
        elif "12:00:00" <= init_time[11:] < "14:00:00":
            init_time = init_time[:11] + "14:00:00"
        elif "00:00:00" <= init_time[11:] < "08:30:00":
            init_time = init_time[:11] + "08:30:00"
        else:
            init_time = (
                str(
                    datetime.datetime.strptime(init_time[:10], "%Y-%m-%d")
                    + datetime.timedelta(days=1)
                )[:11]
                + "08:30:00"
            )
    elif "10-01" <= init_time[5:10] <= "11-15":
        if (
            "08:30:00" <= init_time[11:] < "12:00:00"
            or "13:30:00" <= init_time[11:] < "18:00:00"
        ):
            init_time = init_time
        elif "12:00:00" <= init_time[11:] < "13:30:00":
            init_time = init_time[:11] + "13:30:00"
        elif "00:00:00" <= init_time[11:] < "08:30:00":
            init_time = init_time[:11] + "08:30:00"
        else:
            init_time = (
                str(
                    datetime.datetime.strptime(init_time[:10], "%Y-%m-%d")
                    + datetime.timedelta(days=1)
                )[:11]
                + "08:30:00"
            )
    else:
        if (
            "08:30:00" <= init_time[11:] < "12:00:00"
            or "13:30:00" <= init_time[11:] < "17:30:00"
        ):
            init_time = init_time
        elif "12:00:00" <= init_time[11:] < "13:30:00":
            init_time = init_time[:11] + "13:30:00"
        elif "00:00:00" <= init_time[11:] < "08:30:00":
            init_time = init_time[:11] + "08:30:00"
        else:
            init_time = (
                str(
                    datetime.datetime.strptime(init_time[:10], "%Y-%m-%d")
                    + datetime.timedelta(days=1)
                )[:11]
                + "08:30:00"
            )
    time_tmp = datetime.datetime.strptime(
        init_time, "%Y-%m-%d %H:%M:%S"
    ) + datetime.timedelta(minutes=time1)
    return init_time, time_tmp


def search_sqp(data_set):

    flag, info = prechecking(data_set)
    if flag:
        return info
    else:
        (
            device_index2string,
            worker_index2string,
            worker_string2index,
            device_index,
            worker_index,
            tasks,
        ) = preprocessing(data_set)

        optimization_problem = {
            "name": "XugongTransmission",
            "n_var": tasks * 2,
            "lower": [1, 0] * tasks,
            "upper": [device_index, worker_index] * tasks,
            "dataSet": [data_set],
            "algoResultType": 0,
            "worker_index2string": worker_index2string,
            "worker_string2index": worker_string2index,
        }

        pop_size = 100
        max_fe = 10000
        options = {}
        simulation_request_callback = None

        def run_algo():
            evol_algo = DE(
                pop_size=pop_size,
                options=options,
                optimization_problem=optimization_problem,
                simulation_request_callback=simulation_request_callback,  # noqa
                max_fe=max_fe,
                name="DE-Thread",
                debug=True,
            )
            evol_algo.start()
            evol_algo.join()
            algo_result = evol_algo.get_external_algo_result()

            schedule_result = postprocessing(
                data_set,
                algo_result,
                device_index2string,
                worker_index2string,
                worker_string2index,
            )
            print(schedule_result)
            print("Done")
            return schedule_result

        return run_algo()


if __name__ == "__main__":
    # data_set = pd.read_json(
    #     "/home/xie/xugong/xugong-scheduling/传动-scheduled类型有误1.json"
    # )["data"]
    data_set = pd.read_json("E:\\Desktop\\xugong\\xugong-scheduling\\传动170.json")[
        "data"
    ]
    final_result = search_sqp(data_set)
    print(final_result)
    print("done")

import numpy as np
import pandas as pd


def main(data):
    algo_res = data
    n_obj = 1
    n_var = data.shape[1]-2
    pop = np.array(algo_res)[:, 1:n_var+1].astype(float).astype(int)
    # 最后一列是约束，倒数第二列是目标值
    objv_min = np.argmin(np.array(algo_res)[:, n_var + n_obj])
    return None


if __name__ == '__main__':
    data = pd.read_csv("D:\\WorkSpace\\plat-go\\xugong-scheduling\\full_algo_res.csv")
    main(data)

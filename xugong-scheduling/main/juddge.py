import datetime


def juddge(init_time, time_tmp, time1):  # noqa
    if "02-16" <= init_time[5:10] <= "07-14":
        if (  # noqas
                "08:30:00" <= init_time[11:] < "12:00:00"
                or "13:30:00" <= init_time[11:] < "18:00:00"
        ):
            init_time = init_time
        elif "12:00:00" <= init_time[11:] < "13:30:00":
            init_time = init_time[:11] + "13:30:00"
        elif "00:00:00" <= init_time[11:] < "08:30:00":
            init_time = init_time[:11] + "08:30:00"
        else:  # 从18：00：00-00：00：00
            init_time = (
                    str(
                        datetime.datetime.strptime(init_time[:10], "%Y-%m-%d")
                        + datetime.timedelta(days=1)
                    )[:11]
                    + "08:30:00"
            )
    elif "07-15" <= init_time[5:10] <= "09-30":
        if (
                "08:30:00" <= init_time[11:] < "12:00:00"
                or "14:00:00" <= init_time[11:] < "18:00:00"
        ):
            init_time = init_time
        elif "12:00:00" <= init_time[11:] < "14:00:00":
            init_time = init_time[:11] + "14:00:00"
        elif "00:00:00" <= init_time[11:] < "08:30:00":
            init_time = init_time[:11] + "08:30:00"
        else:
            init_time = (
                    str(
                        datetime.datetime.strptime(init_time[:10], "%Y-%m-%d")
                        + datetime.timedelta(days=1)
                    )[:11]
                    + "08:30:00"
            )
    elif "10-01" <= init_time[5:10] <= "11-15":
        if (
                "08:30:00" <= init_time[11:] < "12:00:00"
                or "13:30:00" <= init_time[11:] < "18:00:00"
        ):
            init_time = init_time
        elif "12:00:00" <= init_time[11:] < "13:30:00":
            init_time = init_time[:11] + "13:30:00"
        elif "00:00:00" <= init_time[11:] < "08:30:00":
            init_time = init_time[:11] + "08:30:00"
        else:
            init_time = (
                    str(
                        datetime.datetime.strptime(init_time[:10], "%Y-%m-%d")
                        + datetime.timedelta(days=1)
                    )[:11]
                    + "08:30:00"
            )
    else:
        if (
                "08:30:00" <= init_time[11:] < "12:00:00"
                or "13:30:00" <= init_time[11:] < "17:30:00"
        ):
            init_time = init_time
        elif "12:00:00" <= init_time[11:] < "13:30:00":
            init_time = init_time[:11] + "13:30:00"
        elif "00:00:00" <= init_time[11:] < "08:30:00":
            init_time = init_time[:11] + "08:30:00"
        else:
            init_time = (
                    str(
                        datetime.datetime.strptime(init_time[:10], "%Y-%m-%d")
                        + datetime.timedelta(days=1)
                    )[:11]
                    + "08:30:00"
            )
    time_tmp = datetime.datetime.strptime(
        init_time, "%Y-%m-%d %H:%M:%S"
    ) + datetime.timedelta(minutes=time1)
    return init_time, time_tmp


if __name__ == '__main__':
    x = [1, 2, 1, 2, 1, 2]
    juddge("2023-03-29 18:01:00", None, 30)
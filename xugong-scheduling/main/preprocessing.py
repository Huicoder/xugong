import numpy as np
import pandas as pd
import json


def process_data_set(data_set):
    device_string2index = dict()
    worker_string2index = dict()
    device_index2string = dict()
    worker_index2string = dict()
    device_index = 1
    worker_index = 0
    tasks = len(data_set["scheduling_task"])

    for item in data_set["scheduling_task"]:
        for i, device_string in enumerate(item["available_device"]):
            if device_string not in device_string2index:
                device_string2index[device_string] = []
                device_index2string[str(device_index)] = []

                value = item["available_device"][device_string]
                for k in range(device_index, device_index + value):
                    device_string2index[device_string] += [k]
                    device_index2string[str(k)] = device_string
                    device_index += 1
            item["available_device"][device_string] = [str(i) for i in
                                                       device_string2index[device_string]]

        for i, worker_string in enumerate(item["available_worker"]):
            if worker_string not in worker_string2index:
                worker_string2index[worker_string] = [worker_index]
                worker_index2string[str(worker_index)] = worker_string
                worker_index += 1
            item["available_worker"][i] = str(worker_string2index[worker_string])
    for item in data_set["scheduled_task"]:
        for i, device_string in enumerate(item["using_device"]):
            if device_string not in device_string2index:
                device_string2index[device_string] = [device_index]
                device_index2string[str(device_index)] = device_string
                device_index += 1
            item["using_device"][i] = str(device_string2index[device_string][0])
        for i, worker_string in enumerate(item["using_worker"]):
            if worker_string not in worker_string2index:
                worker_string2index[worker_string] = [worker_index]
                worker_index2string[str(worker_index)] = worker_string
                worker_index += 1
            item["using_worker"][i] = str(worker_string2index[worker_string][0])

    return device_index2string, worker_index2string, device_index, worker_index, tasks


if __name__ == '__main__':
    # data_set = pd.read_json("E:\\pythonProject\\platgo\\xugong-scheduling\\传动.json")['data']
    data_set = pd.read_json("D:\\WorkSpace\\plat-go\\xugong-scheduling\\传动0728.json")['data']
    (
        device_index2string,
        worker_index2string,
        device_index,
        worker_index,
        tasks,
    ) = process_data_set(data_set)
    data_set.to_json("aaa0728.json")

import numpy as np
import platgo as pg
import pandas as pd


def main(self, N=None):
    if N is None:
        N = self.pop_size

    # data = self.data[0]
    # data = pd.read_json("E:\\pythonProject\\platgo\\xugong-scheduling\\aaa.json")['data']
    data = pd.read_json("D:\\WorkSpace\\plat-go\\xugong-scheduling\\aaa.json")['data']
    dec = []
    for k in range(N):
        dec1 = []
        for i in range(len(data['scheduling_task'])):
            # 给第i个实验随机分配一个符合要求的实验台
            experiment_i = np.random.choice(data['scheduling_task'][i]['available_device'])
            # 给第i个实验根据已经分配的实验台再随机分配一个符合要求的实验员
            pop_i = np.random.choice(data['scheduling_task'][i]['available_worker'])
            dec1 += [experiment_i, pop_i]
        dec += [dec1]
    dec = np.array(dec, dtype=np.float64)
    return pg.Population(decs=dec)


if __name__ == '__main__':
    main(pg.problems.TSP(), 10)

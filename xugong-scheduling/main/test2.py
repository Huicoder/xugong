import json
import random
'''
      {
        "available_device": [
          "11024"
        ],
        "available_worker": [
          "68"
        ],
        "experiment_id": "1088939404472352768",
        "laboratory": "CD",
        "plan_end_time": "2023-04-05 15:10:00",
        "plan_start_time": "2023-04-05 14:20:00",
        "task_duration": 50,
        "task_id": "1088939421308289024",
        "task_order": 5
      }
'''
dict1 = []
for i in range(1500):
    dict2 = {}
    dict2.setdefault("available_device", [str(i) for i in range(20)])
    dict2.setdefault("available_worker", [str(i) for i in range(20)])
    dict2.setdefault("experiment_id", "1088939404472352768")
    dict2.setdefault("laboratory", "CD")
    dict2.setdefault("task_duration", random.randint(1, 50))
    dict2.setdefault("task_id", i+1)
    dict2.setdefault("task_order", random.randint(1, 10))
    dict1.append(dict2)
json_string = json.dumps(dict1)
with open("data.json", "w", encoding = 'utf-8') as file:
    file.write(json_string)
print("done")

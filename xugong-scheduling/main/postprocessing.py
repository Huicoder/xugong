import numpy as np
import pandas as pd
import datetime


def process_data_set(data_set):
    device_string2index = dict()
    worker_string2index = dict()
    device_index2string = dict()
    worker_index2string = dict()
    device_index = 1
    worker_index = 0
    tasks = len(data_set["scheduling_task"])

    for item in data_set["scheduled_task"]:
        for i, device_string in enumerate(item["using_device"]):
            if device_string not in device_string2index:
                device_string2index[device_string] = device_index
                device_index2string[str(device_index)] = device_string
                device_index += 1
            item["using_device"][i] = str(device_string2index[device_string])
        for i, worker_string in enumerate(item["using_worker"]):
            if worker_string not in worker_string2index:
                worker_string2index[worker_string] = worker_index
                worker_index2string[str(worker_index)] = worker_string
                worker_index += 1
            item["using_worker"][i] = str(worker_string2index[worker_string])

    for item in data_set["scheduling_task"]:
        for i, device_string in enumerate(item["available_device"]):
            if device_string not in device_string2index:
                device_string2index[device_string] = device_index
                device_index2string[str(device_index)] = device_string
                device_index += 1
            item["available_device"][i] = str(device_string2index[device_string])
        for i, worker_string in enumerate(item["available_worker"]):
            if worker_string not in worker_string2index:
                worker_string2index[worker_string] = worker_index
                worker_index2string[str(worker_index)] = worker_string
                worker_index += 1
            item["available_worker"][i] = str(worker_string2index[worker_string])
    return {
        "deviceIndex2String": device_index2string,
        "workerIndex2String": worker_index2string,
        "optimizationProblem": {
            "n_var": tasks * 2,
            "lower": [1, 0] * tasks,
            "upper": [device_index, worker_index] * tasks,
            "dataSet": [data_set],
            "algoResultType": 0,
        },
    }


def pan(
        device,
        worker,
        data,
        using_worker,
        using_device,
        init_time,
        time_tmp,
        time1,
        time_list,
        pop_dict,
):
    """
    判断待排任务与固定任务是否冲突，时间冲突则进行调整
    :param device: 待排任务占用的设备id
    :param worker: 待排任务占用的人员id
    :param data: 数据集
    :param using_worker: 每个固定任务所需人员id
    :param using_device: 每个固定任务所需设备id
    :param init_time: 数据集中给出的开始时间
    :param time_tmp: 该待排任务的结束时间
    :param time1: 该待排任务的实验时长
    :param time_list: # 每个设备的使用时间段
    :param pop_dict:  # 每个人员的占用时间段
    :return: 该待排任务的在解决冲突后的开始时间和结束时间
    """
    time = []
    # 在固定任务中找出与当前待排任务相同的人员的开始时间和结束时间
    # 其实也在固定任务中找出当前待排任务所需人员的所有的工作时间段
    for i in np.where(using_worker == str(int(worker)))[0]:
        time.append(
            [
                data["scheduled_task"][i]["start_time"],
                data["scheduled_task"][i]["end_time"],
            ]
        )
    # 在固定任务中找出与当前待排任务相同的设备的开始时间和结束时间
    # 其实也在固定任务中找出当前待排任务所需设备的所有工作时间段
    for i in np.where(using_device == str(int(device)))[0]:
        time.append(
            [
                data["scheduled_task"][i]["start_time"],
                data["scheduled_task"][i]["end_time"],
            ]
        )
    if str(int(device)) in time_list:
        for i in time_list[str(int(device))]:
            time.append(i)
    if str(int(worker)) in pop_dict:
        for i in pop_dict[str(int(worker))]:
            time.append(i)
    time.sort(key=lambda x: (x[0], x[1]))
    flag = True  # 判断待排任务是否固定 True为不固定
    # 根据时间表调整当前待排任务的开始时间和结束时间
    init_time, time_tmp = juddge(init_time, time_tmp, time1)
    for i in range(len(time)):
        # 判断当前的待排任务与固定任务的时间是否冲突
        if (
                time[i][0] < init_time < time[i][1]
                or time[i][0] < str(time_tmp) < time[i][1]
        ):
            init_time = time[i][1]  # 时间冲突，将待排任务的开始时间设置发生冲突固定任务的结束时间
        elif init_time <= time[i][0] and str(time_tmp) >= time[i][1]:
            init_time = time[i][1]  # 时间冲突，将待排任务的开始时间设置发生冲突固定任务的结束时间
        elif str(time_tmp) <= time[i][0]:  # 待排任务的结束时间在固定任务开始时间之前，此时待排任务的位置就可以固定了
            flag = False  # 待排任务和固定任务没有发生时间冲突
        # 解决时间冲突后，计算当前待排任务的实际的开始时间和结束时间
        init_time, time_tmp = juddge(init_time, time_tmp, time1)
        if not flag:  # flag为False说明待排任务已经解决与固定任务的时间冲突，且位置已经固定
            break
    return init_time, time_tmp


def juddge(init_time, time_tmp, time1):
    """
    根据时间表调整待排任务的开始时间和结束时间
    :param init_time: 待排任务的开始时间
    :param time_tmp: 待排任务的结束时间
    :param time1:  待排任务的实验时间
    :return:  调整后的开始时间和结束时间
    """
    if "02-16" <= init_time[5:10] <= "07-14":
        if (  # noqas
                "08:30:00" <= init_time[11:] < "12:00:00"
                or "13:30:00" <= init_time[11:] < "18:00:00"
        ):
            init_time = init_time
        elif "12:00:00" <= init_time[11:] < "13:30:00":
            init_time = init_time[:11] + "13:30:00"
        elif "00:00:00" <= init_time[11:] < "08:30:00":
            init_time = init_time[:11] + "08:30:00"
        else:
            init_time = (
                    str(
                        datetime.datetime.strptime(init_time[:10], "%Y-%m-%d")
                        + datetime.timedelta(days=1)
                    )[:11]
                    + "08:30:00"
            )
    elif "07-15" <= init_time[5:10] <= "09-30":
        if (
                "08:30:00" <= init_time[11:] < "12:00:00"
                or "14:00:00" <= init_time[11:] < "18:00:00"
        ):
            init_time = init_time
        elif "12:00:00" <= init_time[11:] < "14:00:00":
            init_time = init_time[:11] + "14:00:00"
        elif "00:00:00" <= init_time[11:] < "08:30:00":
            init_time = init_time[:11] + "08:30:00"
        else:
            init_time = (
                    str(
                        datetime.datetime.strptime(init_time[:10], "%Y-%m-%d")
                        + datetime.timedelta(days=1)
                    )[:11]
                    + "08:30:00"
            )
    elif "10-01" <= init_time[5:10] <= "11-15":
        if (
                "08:30:00" <= init_time[11:] < "12:00:00"
                or "13:30:00" <= init_time[11:] < "18:00:00"
        ):
            init_time = init_time
        elif "12:00:00" <= init_time[11:] < "13:30:00":
            init_time = init_time[:11] + "13:30:00"
        elif "00:00:00" <= init_time[11:] < "08:30:00":
            init_time = init_time[:11] + "08:30:00"
        else:
            init_time = (
                    str(
                        datetime.datetime.strptime(init_time[:10], "%Y-%m-%d")
                        + datetime.timedelta(days=1)
                    )[:11]
                    + "08:30:00"
            )
    else:
        if (
                "08:30:00" <= init_time[11:] < "12:00:00"
                or "13:30:00" <= init_time[11:] < "17:30:00"
        ):
            init_time = init_time
        elif "12:00:00" <= init_time[11:] < "13:30:00":
            init_time = init_time[:11] + "13:30:00"
        elif "00:00:00" <= init_time[11:] < "08:30:00":
            init_time = init_time[:11] + "08:30:00"
        else:
            init_time = (
                    str(
                        datetime.datetime.strptime(init_time[:10], "%Y-%m-%d")
                        + datetime.timedelta(days=1)
                    )[:11]
                    + "08:30:00"
            )
    time_tmp = datetime.datetime.strptime(
        init_time, "%Y-%m-%d %H:%M:%S"
    ) + datetime.timedelta(minutes=time1)
    return init_time, time_tmp


def count_Need_PeopleAndAdvice(data1):
    """
    统计固定任务和待排任务所需人员和设备的id
    :param data1: 数据集
    :return: 人员id和设备id（去重后的）
    """
    available_device = []
    available_pop = []
    i = max(len(data1["scheduling_task"]), len(data1["scheduled_task"]))
    for k in range(i):
        if k < len(data1["scheduling_task"]):
            available_device.append(
                data1["scheduling_task"][k]["available_device"]
            )
            available_pop.append(data1["scheduling_task"][k]["available_worker"])
        if k < len(data1["scheduled_task"]):
            available_pop.append(data1["scheduled_task"][k]["using_worker"])
            available_device.append(data1["scheduled_task"][k]["using_device"])
    available_device = list(set(sum(available_device, [])))  # 降维->去重->转为列表
    available_pop = list(set(sum(available_pop, [])))  # sum(x,[]) 也可以用于列表的展开，效果相当于各子列表相加(降维)
    return available_device, available_pop


def find_EarliestTime_InSameExperiment(data1, algo_res):
    """
    找出相同项目下的最早开始的任务时间和优先级
    :param data1: 数据集
    :param algo_res: 算法结果
    :return: 相同项目下的最早开始的任务时间和优先级
    """
    D = algo_res.shape[1]-1
    experiment_id = {}  # {'1088939404472352768': [('2023-03', 5)]} 项目id，一个项目下会有多个任务id
    if len(data1["scheduling_task"]) != 0:
        if data1["scheduling_task"][0]["laboratory"] == "CD":  # 判断待排任务是否属于传动实验室
            for j in range(0, D, 2):  # 对决策变量的维度循环（实际上对待排任务进行遍历）
                if "plan_start_time" in data1["scheduling_task"][int(j / 2)]:  # 有计划开始时间的待排任务
                    tt = data1["scheduling_task"][int(j / 2)]["experiment_id"]
                    if data1["scheduling_task"][int(j / 2)]["experiment_id"] not in experiment_id:
                        experiment_id.setdefault(tt, []).append(
                            (
                                data1["scheduling_task"][int(j / 2)]["plan_start_time"][0:7],
                                data1["scheduling_task"][int(j / 2)]["task_order"],
                            )
                        )
                    else:  # 是一个相同项目下的待排任务
                        if (
                                data1["scheduling_task"][int(j / 2)]["plan_start_time"][0:7] <
                                experiment_id[data1["scheduling_task"][int(j / 2)]["experiment_id"]][0][0]
                        ):  # 在同一个项目下当前待排任务的开始时间先于前一个待排任务的
                            tt = data1["scheduling_task"][int(j / 2)]["experiment_id"]
                            experiment_id[tt] = [
                                (
                                    data1["scheduling_task"][int(j / 2)]["plan_start_time"][0:7],
                                    data1["scheduling_task"][int(j / 2)]["task_order"],
                                )
                            ]
                else:  # 没有计划开始时间的待排任务
                    tt = data1["scheduling_task"][int(j / 2)]["experiment_id"]
                    if data1["scheduling_task"][int(j / 2)]["experiment_id"] not in experiment_id:
                        experiment_id.setdefault(tt, []).append(
                            ("3000-13", data1["scheduling_task"][int(j / 2)]["task_order"])
                        )
    return experiment_id


def group_TaskToAdvice(data1, available_device, experiment_id, algo_res):
    """
    由于设备是可以并行工作，先将每个任务按照设备分为各个组
    :param data1: 数据集
    :param available_device: 使用的设备集合
    :param experiment_id: 每个项目下的任务的最早开始时间和优先级
    :param algo_res: 算法结果
    :return: 分组后的人员使用表，任务顺序表，实验时间表
    """
    n_obj = 1
    n_var = algo_res.shape[1]-1
    pop = np.array(algo_res)[:, :n_var].astype(float).astype(int)
    # 最后一列是约束，倒数第二列是目标值
    objv_min = np.argmin(np.array(algo_res)[:, n_var])
    x = pop[objv_min]
    pop_i = []  # 同一设备下的人员id
    experiment = []  # 同一设备下任务顺序
    time = []  # 同一设备下每个任务的实验时间
    # 将每个任务按照设备分为各个组
    for k in range(1, len(available_device) + 1):  # 对每个设备进行循环
        experiment_time = []  # 同一项目下实验时间
        people = []  # 同一项目下的人员顺序
        order = []  # 同一项目下的任务优先级顺序
        seq1 = []  # 同一项目下的任务开始时间和优先级[('2023-03', 5, '1088939404472352768')]
        t = []  # json文件中的实验顺序  从0开始
        # 先遍历所有任务，将其计划时间、任务优先级都修改同一个项目下最高的那一个，
        # 以保证一个项目下的所有任务排序时都将连续
        for j in range(0, n_var, 2):  # 对待排任务循环
            if x[j] == k:  # 判断设备是否是所需设备
                people.append(x[j + 1])  # 如果是所需设备，则进行人员添加
                experiment_time.append(
                    data1["scheduling_task"][int(j / 2)]["task_duration"]
                )
                tt = data1["scheduling_task"][int(j / 2)]["experiment_id"]
                if "plan_start_time" in data1["scheduling_task"][int(j / 2)]:  # 有计划开始时间的待排任务
                    if data1["scheduling_task"][int(j / 2)]["laboratory"] == "CD":  # 该待排任务属于传动实验室
                        # experiment_id[tt]->[('2023-03', 5)]
                        seq1.append((experiment_id[tt][0][0], experiment_id[tt][0][1], tt))
                    else:  # 该待排任务属于普通实验室
                        seq1.append((data1['scheduling_task'][int(j / 2)]['plan_start_time'][0:7],  # noqa
                                     data1['scheduling_task'][int(j / 2)]['task_order'], tt))  # noqa
                else:  # 没有计划开始时间的待排任务
                    if data1["scheduling_task"][int(j / 2)]["laboratory"] == "CD":  # 该待排任务属于传动实验室
                        seq1.append(("3000-13", experiment_id[tt][0][1], tt))
                    else:  # 该待排任务属于普通实验室
                        seq1.append(("3000-13", data1['scheduling_task'][int(j / 2)]['task_order'], tt))
                order.append(data1["scheduling_task"][int(j / 2)]["task_order"])
                t.append(int(j / 2))
        # 根据元组第0个值升序排序，若第0个值相等则根据第1个值升序排序
        # 先按照年月份排序再按照优先级排序
        tmp1 = sorted(range(len(t)), key=lambda x: (seq1[x][0], seq1[x][1]))
        pop_i.append(np.array(people)[tmp1])
        experiment.append(np.array(t)[tmp1])
        time.append(np.array(experiment_time)[tmp1])
    pop_i = pd.DataFrame(pop_i)
    experiment = pd.DataFrame(experiment, dtype=np.int32)
    time = pd.DataFrame(time)
    return pop_i, experiment, time


def set_SchedulingTaskTime(data1, init_time, pop_i, experiment, time):
    """
    固定待排任务的在各自设备上的开始时间和结束时间
    :param data1: 数据集
    :param init_time: 开始时间
    :param pop_i: 人员表
    :param experiment: 任务表
    :param time: 实验时间表
    :return: 每个设备及人员的占用时间段
    """
    time_list = {}  # 记录每个设备的占用时间段{'1': [['2023-03-30 10:20:00', '2023-03-30 11:10:00']]}
    pop_dict = {}  # 记录每个人员的占用时间段{'2': [['2023-03-30 10:20:00', '2023-03-30 11:10:00']]}
    using_worker = []  # 固定任务所需人员 [list(['0']) list(['1']) list(['2'])]->['0' '1' '2']
    using_device = []  # 固定任务所需设备 [list(['1']) list(['2']) list(['1'])]->['1' '2' '1']
    if len(data1["scheduled_task"]):  # 判断是否有固定任务
        using_worker = np.array(
            pd.DataFrame(data1["scheduled_task"])["using_worker"]
        )
        using_device = np.array(
            pd.DataFrame(data1["scheduled_task"])["using_device"]
        )
    for k in range(len(using_device)):  # len的长度就是固定任务的长度
        using_worker[k] = using_worker[k][0]
        using_device[k] = using_device[k][0]
    for h in range(time.shape[1]):  # 根据设备将待排任务分组后形成的表的列数
        s = 1  # 记录设备id
        for h1 in range(time.shape[0]):  # 并行设备的个数
            if h == 0:  # 从第一列开始计算每个任务的开始时间
                if ~np.isnan(pop_i[h][h1]):
                    if str(int(pop_i[h][h1])) not in pop_dict:  # 该待排任务的所需人员不在pop_dict中
                        init_time1 = init_time
                        time_tmp = datetime.datetime.strptime(
                            init_time1, "%Y-%m-%d %H:%M:%S"
                        ) + datetime.timedelta(
                            minutes=data1["scheduling_task"][
                                int(experiment[h][h1])
                            ]["task_duration"]
                        )
                    else:
                        init_time1 = pop_dict[str(int(pop_i[h][h1]))][-1][1]
                        time_tmp = datetime.datetime.strptime(
                            init_time1, "%Y-%m-%d %H:%M:%S"
                        ) + datetime.timedelta(
                            minutes=data1["scheduling_task"][
                                int(experiment[h][h1])
                            ]["task_duration"]
                        )
                    init_time1, time_tmp = pan(
                        h1 + 1,
                        pop_i[h][h1],
                        data1,
                        using_worker,
                        using_device,
                        init_time1,
                        time_tmp,
                        data1["scheduling_task"][int(experiment[h][h1])][
                            "task_duration"
                        ],
                        time_list,
                        pop_dict,
                    )
                    time_list.setdefault(str(int(s)), []).append(
                        [init_time1, str(time_tmp)]
                    )
                    pop_dict.setdefault(str(int(pop_i[h][h1])), []).append(
                        [init_time1, str(time_tmp)]
                    )
                s += 1
            else:
                if ~np.isnan(pop_i[h][h1]):
                    if str(int(pop_i[h][h1])) not in pop_dict:
                        init_time1 = time_list[str(s)][-1][1]
                    else:
                        init_time1 = max(
                            time_list[str(s)][-1][1],
                            pop_dict[str(int(pop_i[h][h1]))][-1][1],
                        )  # 找出上一个待排任务的结束时间
                    time_tmp = datetime.datetime.strptime(
                        init_time1, "%Y-%m-%d %H:%M:%S"
                    ) + datetime.timedelta(
                        minutes=data1["scheduling_task"][
                            int(experiment[h][h1])
                        ]["task_duration"]
                    )
                    init_time1, time_tmp = pan(
                        h1 + 1,
                        pop_i[h][h1],
                        data1,
                        using_worker,
                        using_device,
                        init_time1,
                        time_tmp,
                        data1["scheduling_task"][int(experiment[h][h1])][
                            "task_duration"
                        ],
                        time_list,
                        pop_dict,
                    )
                    time_list.setdefault(str(int(s)), []).append(
                        [init_time1, str(time_tmp)]
                    )
                    pop_dict.setdefault(str(int(pop_i[h][h1])), []).append(
                        [init_time1, str(time_tmp)]
                    )
                s += 1
    return time_list, pop_dict


def cal_Toaltime(init_time, time_list, available_device):
    """
    计算最终目标时间，即最后一个待排任务的结束时间
    :param init_time: 数据集给出的开始时间
    :param time_list: 每个设备的占用时间段
    :param available_device: 记录设备的id的列表
    :return: 结束时间
    """
    f = init_time
    for q in range(1, len(available_device) + 1):
        if str(q) in time_list:
            if f <= time_list[str(q)][-1][1]:
                f = time_list[str(q)][-1][1]
    f = datetime.datetime.strptime(f, "%Y-%m-%d %H:%M:%S") - datetime.datetime.strptime(init_time,
                                                                                        "%Y-%m-%d %H:%M:%S")
    f = f.days * 24 * 3600 + f.seconds  # 秒当作目标值,从初始时间到最后一个待排任务的结束时间
    return f


def print_Result(time, experiment, data, time_list, pop_i, device_index2string, worker_index2string):
    """
    输出结果即每一个待排任务的信息
    :param time:
    :param experiment:
    :param data:
    :param time_list:
    :param pop_i:
    :param device_index2string:
    :param worker_index2string:
    :return:
    """
    outcome = []
    for h in range(time.shape[0]):
        for h1 in range(time.shape[1]):
            if ~np.isnan(experiment.loc[h][h1]):
                tmp = {
                    "experiment_id": data["scheduling_task"][
                        int(experiment.loc[h][h1])
                    ]["experiment_id"],
                    "laboratory": data["scheduling_task"][
                        int(experiment.loc[h][h1])
                    ]["laboratory"],
                    "task_id": data["scheduling_task"][int(experiment.loc[h][h1])][
                        "task_id"
                    ],
                    "start_time": time_list[str(h + 1)][h1][0],
                    "end_time": time_list[str(h + 1)][h1][1],
                    "using_device": [device_index2string[str(h + 1)]],
                    "using_worker": [
                        worker_index2string[str(int(pop_i.loc[h][h1]))]
                    ],
                }
                outcome.append(tmp)
    return outcome


def test(outcome, data, worker_index2string, device_index2string):
    """
    数据校验：同一人员是否出现时间冲突，同一设备是否出现时间冲突
    :param outcome: 数据处理后的输出
    :param data: 数据集
    :param worker_index2string: 人员下标对应表：{'0': 'w_0', '1': '1'}
    :param device_index2string: 设备下标对应表
    :return: 数据校验后的输出结果
    """
    people_time = {}
    people_time1 = {}
    device_time = {}
    device_time1 = {}
    for i in outcome:
        people = i['using_worker'][0]
        if people not in people_time:
            people_time.setdefault(people, []).append([i['start_time'], i['end_time'], i['task_id'], "scheduling_task", i['using_device'][0], people])  # noqa
            people_time1.setdefault(people, []).append([i['start_time'], i['end_time'], i['task_id'], "scheduling_task", i['using_device'][0], people])# noqa
        else:
            people_time[people].append([i['start_time'], i['end_time'], i['task_id'], 'scheduling_task', i['using_device'][0], people]) # noqa
            people_time1[people].append([i['start_time'], i['end_time'], i['task_id'], 'scheduling_task', i['using_device'][0], people]) # noqa
        device = i['using_device'][0]
        if device not in device_time:
            device_time.setdefault(device, []).append([i['start_time'], i['end_time'], i['task_id'], "scheduling_task", i['using_device'][0], people])# noqa
            device_time1.setdefault(device, []).append([i['start_time'], i['end_time'], i['task_id'], "scheduling_task", i['using_device'][0], people])# noqa
        else:
            device_time[device].append([i['start_time'], i['end_time'], i['task_id'], 'scheduling_task', i['using_device'][0], people])# noqa
            device_time1[device].append([i['start_time'], i['end_time'], i['task_id'], 'scheduling_task', i['using_device'][0], people])# noqa
    for i in data['scheduled_task']:  # 遍历固定任务
        people = worker_index2string[i['using_worker'][0]]  # 该固定任务进行数据处理前的所用人员
        if people not in people_time:
            people_time.setdefault(people, []).append([i['start_time'], i['end_time'], i['task_id'], "scheduled_task", device, people])# noqa
            people_time1.setdefault(people, []).append([i['start_time'], i['end_time'], i['task_id'], "scheduled_task", device, people])# noqa
        else:
            people_time[people].append([i['start_time'], i['end_time'], i['task_id'], 'scheduled_task', device, people])# noqa
            people_time1[people].append([i['start_time'], i['end_time'], i['task_id'], 'scheduled_task', device, people])# noqa
        device = device_index2string[i['using_device'][0]]  # 该固定任务进行数据处理前的所用设备
        if device not in device_time:
            device_time.setdefault(device, []).append([i['start_time'], i['end_time'], i['task_id'], "scheduled_task", device, people])# noqa
            device_time1.setdefault(device, []).append([i['start_time'], i['end_time'], i['task_id'], "scheduled_task", device, people])# noqa
        else:
            device_time[device].append([i['start_time'], i['end_time'], i['task_id'], 'scheduled_task', device, people])# noqa
            device_time1[device].append([i['start_time'], i['end_time'], i['task_id'], 'scheduled_task', device, people])# noqa
    for i in people_time:
        time = people_time[i]
        time.sort(key=lambda x: (x[0], x[1]))
        j = 0
        while j < len(time):
            if time[j][3] == "scheduling_task":
                tmp = time[j]
                del time[j]  # 删除列表time中的第j个元素
                for k in time:
                    tmp[0] = '2023-03-31 08:31:00'
                    tmp[1] = '2023-03-31 08:32:00'
                    if overlap(k, tmp):  # 如果出现某个人员的时间段重叠
                        print('重叠了')
                        task_duration = datetime.datetime.strptime(tmp[1],"%Y-%m-%d %H:%M:%S") - datetime.datetime.strptime(tmp[0],"%Y-%m-%d %H:%M:%S") # noqa
                        tmp[0] = str(time[-1][1])  # 重新设置人员占用的开始时间
                        tmp[1] = str(datetime.datetime.strptime(time[-1][1], "%Y-%m-%d %H:%M:%S") + task_duration)  # 结束时间也随之改变
                        tmp[0], tmp[1] = juddge(tmp[0], tmp[1], task_duration.total_seconds() / 60)  # 根据时间表重新安排时间
                        tmp[1] = str(tmp[1])
                        tmp[0], tmp[1] = update_device(tmp, device_time, task_duration)  # 更新设备中的该任务的时间
                        outcome = update_outcome(tmp, outcome)  # 更新输出
            else:
                j += 1
    people_time = people_time1
    device_time = device_time1
    for i in device_time:
        time = device_time[i]
        time.sort(key=lambda x: (x[0], x[1]))
        j = 0
        while j < len(time):
            if time[j][3] == "scheduling_task":
                tmp = time[j]
                del time[j]
                for k in time:
                    # tmp[1] = '2023-06-20 16:00:00'
                    if overlap(k, tmp):
                        print('重叠了')
                        task_duration = datetime.datetime.strptime(tmp[1],"%Y-%m-%d %H:%M:%S") - \
                            datetime.datetime.strptime(tmp[0],"%Y-%m-%d %H:%M:%S") # noqa
                        tmp[0] = str(time[-1][1])
                        tmp[1] = str(datetime.datetime.strptime(time[-1][1],"%Y-%m-%d %H:%M:%S") + task_duration) # noqa
                        tmp[0], tmp[1] = juddge(tmp[0], tmp[1], task_duration.total_seconds() / 60) # noqa
                        tmp[1] = str(tmp[1])
                        tmp[0], tmp[1] = update_people(tmp, people_time, task_duration)
                        outcome = update_outcome(tmp, outcome)
            else:
                j += 1
    return outcome


def update_device(tmp, device_time, task_duration):
    """
    如果出现人员占用时间冲突，更新设备时间表
    :param tmp: 该人员的新的占用时间
    :param device_time: 设备时间表
    :param task_duration: 任务时长
    :return: 更新设备时间表后的该人员的占用时间
    """
    i = device_time[tmp[-2]].copy()
    a = np.array(i)
    index = np.where(a[:, 2] == tmp[2])[0][0]
    i[index][0],  i[index][1] = tmp[0], tmp[1]
    tmp = i[index]
    del i[index]
    i.sort(key=lambda x: (x[0], x[1]))
    for j in i:
        # i[-1][1] = '2023-06-30 11:00:00'
        if overlap(tmp, j):  # 判断更新后的占用该设备时间与剩余的时间段是否冲突
            print("重叠")
            tmp[0] = i[-1][1]
            tmp[1] = str(datetime.datetime.strptime(i[-1][1],"%Y-%m-%d %H:%M:%S") + task_duration)# noqa
            tmp[0], tmp[1] = juddge(tmp[0], tmp[1], task_duration.total_seconds() / 60)
            tmp[1] = str(tmp[1])
    return tmp[0], tmp[1]


def update_people(tmp, people_time, task_duration):
    i = people_time[tmp[-1]].copy()
    a = np.array(i)
    index = np.where(a[:, 2] == tmp[2])[0][0]
    i[index][0],  i[index][1] = tmp[0], tmp[1]
    tmp = i[index]
    del i[index]
    i.sort(key=lambda x: (x[0], x[1]))
    for j in i:
        # i[-1][1] = '2023-06-30 11:00:00'
        if overlap(tmp, j):
            print("重叠")
            tmp[0] = i[-1][1]
            tmp[1] = str(datetime.datetime.strptime(i[-1][1],"%Y-%m-%d %H:%M:%S") + task_duration)# noqa
            tmp[0], tmp[1] = juddge(tmp[0], tmp[1], task_duration.total_seconds() / 60)
            tmp[1] = str(tmp[1])
    return tmp[0], tmp[1]


def update_outcome(tmp, outcome):
    for i in outcome:
        if i['task_id'] == tmp[2]:
            i['start_time'] = tmp[0]
            i['end_time'] = tmp[1]
    return outcome


def overlap(k, tmp):
    """
    判断时间是否重叠
    :param k: 时间表中的一个时间
    :param tmp: 人员的占用时间
    :return: True为重叠，False不重叠
    """
    if k[0] < tmp[0] < k[1] or \
            k[0] < tmp[1] < k[1] or \
            tmp[0] < k[0] < tmp[1] or \
            tmp[0] < k[1] < tmp[1] or \
            (k[0] <= tmp[0] and k[1] >= tmp[1]) or \
            (tmp[0] <= k[0] and tmp[1] >= k[1]):
        return True
    else:
        return False


def main(data_set):

    preprocessing_res = process_data_set(data_set)
    data = preprocessing_res["optimizationProblem"]["dataSet"][0]
    device_index2string = preprocessing_res["deviceIndex2String"]
    worker_index2string = preprocessing_res["workerIndex2String"]
    # 算法优化结果
    algo_res = pd.read_csv("D:\\WorkSpace\\plat-go\\xugong-scheduling\\full_algo_res.csv")
    # 获取开始时间
    init_time = data.get("schedule_start_time", "2023-01-19 00:00:00")
    # 统计所需人员和设备id
    available_device, available_pop = count_Need_PeopleAndAdvice(data)
    # 同一项目下的最早开始的任务时间和优先级
    experiment_id = find_EarliestTime_InSameExperiment(data, algo_res)
    # 将每个任务按照设备分为各个组
    pop_i, experiment, time = group_TaskToAdvice(data, available_device, experiment_id, algo_res)
    # 固定待排任务的开始和结束时间
    time_list, pop_dict = set_SchedulingTaskTime(data, init_time, pop_i, experiment, time)
    f = cal_Toaltime(init_time, time_list, available_device)
    # 输出结果
    outcome = print_Result(time, experiment, data, time_list, pop_i, device_index2string, worker_index2string)
    # 对结果进行校验
    outcome_test = test(outcome, data, worker_index2string, device_index2string)
    print("done")


if __name__ == '__main__':
    data_set = pd.read_json("D:\\WorkSpace\\plat-go\\xugong-scheduling\\aaa1.json")["data"]
    main(data_set)

import random

import numpy as np
import pandas as pd
import json


def process_data_set(data_set):
    device_string2index = dict()
    worker_string2index = dict()
    device_index2string = dict()
    worker_index2string = dict()
    device_index = 1
    worker_index = 0
    tasks = len(data_set["scheduling_task"])

    for item in data_set["scheduling_task"]:
        for i, device_string in enumerate(item["available_device"]):
            if device_string not in device_string2index:
                device_string2index[device_string] = []
                device_index2string[str(device_index)] = []

                device_value = item["available_device"][device_string]
                for k in range(device_index, device_index + device_value):
                    device_string2index[device_string] += [k]
                    device_index2string[str(k)] = device_string
                    device_index += 1
            item["available_device"][device_string] = [str(i) for i in
                                                       device_string2index[device_string]]
        item["available_device"] = sum(list(map(lambda x: x, item["available_device"].values())), [])
        for i, worker_string in enumerate(item["available_worker"]):
            if worker_string not in worker_string2index:
                worker_string2index[worker_string] = []
                worker_index2string[str(worker_index)] = []

                worker_value = item['available_worker'][worker_string]
                for k in range(worker_index, worker_index+worker_value):
                    worker_string2index[worker_string] += [k]
                    worker_index2string[str(k)] = worker_string
                    worker_index += 1
            item["available_worker"][worker_string] = [str(i) for i in
                                                       worker_string2index[worker_string]]
        # 使用lambda函数结合map()方法获取字典中的所有值,并进行列表合并
        if item["task_type"] == "可靠性":
            item["available_worker"] = sum(list(map(lambda x: x, item["available_worker"].values())), [])
        else:
            item["available_worker"] = list(map(lambda x: x, item["available_worker"].values()))

    for item in data_set["scheduled_task"]:
        for i, device_string in enumerate(item["using_device"]):
            if device_string not in device_string2index:
                device_string2index[device_string] = []
                device_index2string[str(device_index)] = []
                device_value = item["using_device"][device_string]
                for k in range(device_index, device_index + device_value):
                    device_string2index[device_string] += [k]
                    device_index2string[str(k)] = device_string
                    device_index += 1

            item["using_device"] = [str(int(random.choice(device_string2index[device_string])))]

        for i, worker_string in enumerate(item["using_worker"]):
            if worker_string not in worker_string2index:
                worker_string2index[worker_string] = []
                worker_index2string[str(worker_index)] = []
                worker_value = item['using_worker'][worker_string]
                for k in range(worker_index, worker_index+worker_value):
                    worker_string2index[worker_string] += [k]
                    worker_index2string[str(k)] = worker_string
                    worker_index += 1

            if item["task_type"] == "可靠性":
                item["using_worker"] = [str(random.choice(worker_string2index[worker_string]))]
            else:
                item["using_worker"] = [[str(i) for i in worker_string2index[worker_string]]]

    return device_index2string, worker_index2string, device_index, worker_index, tasks


if __name__ == '__main__':
    # data_set = pd.read_json("E:\\pythonProject\\platgo\\xugong-scheduling\\传动.json")['data']
    data_set = pd.read_json("D:\\WorkSpace\\plat-go\\xugong-scheduling\\传动0728.json")['data']
    (
        device_index2string,
        worker_index2string,
        device_index,
        worker_index,
        tasks,
    ) = process_data_set(data_set)
    data_set.to_json("aaa0728.json", force_ascii=False)
    print("done")

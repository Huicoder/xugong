import json
import random
from datetime import datetime, timedelta

FIXED_EXPERIMENT_IDS = ["id1", "id2", "id3", "id4", "id5", "id6", "id7", "id8", "id9", "id10"]
FIXED_USING_DEVICE_RESULTS = [{f"device{i}": random.randint(1, 10)} for i in range(1, 11)]
# 固定的 using_worker_result 值字典，值为随机数
FIXED_USING_WORKER_RESULTS = [{f"worker{i}": random.randint(1, 10)} for i in range(1, 11)]

def generate_random_data():
    # Generate random date and time within a range
    start_time = datetime(2023, 8, 23, 15, 40, 0) + timedelta(days=random.randint(1, 5), hours=random.randint(1, 10),
                                                              minutes=random.randint(1, 59))
    end_time = start_time + timedelta(days=random.randint(1, 5), hours=random.randint(1, 10),
                                      minutes=random.randint(1, 59))
    task_types = ["性能", "可靠性"]
    # Construct the data dictionary
    data = {
        "start_time": start_time.strftime("%Y-%m-%d %H:%M:%S"),
        "end_time": end_time.strftime("%Y-%m-%d %H:%M:%S"),
        "experiment_id": random.choice(FIXED_EXPERIMENT_IDS),
        "laboratory": "CD",
        "task_id": str(random.randint(10 ** 18, 10 ** 19 - 1)),
        "task_sort": random.randint(1, 20),
        "task_type": random.choice(task_types),
        "using_device": random.choice(FIXED_USING_DEVICE_RESULTS),
        "using_worker": random.choice(FIXED_USING_WORKER_RESULTS)
    }
    return data


def generate_random_data2():
    # 固定的 experiment_id 值列表

    # Generate random date and time within a range
    plan_start_time = datetime(2023, 8, 23, 15, 40, 0) + timedelta(days=random.randint(1, 5),
                                                                   hours=random.randint(1, 10),
                                                                   minutes=random.randint(1, 59))
    task_duration = random.randint(50, 100)
    plan_end_time = (plan_start_time + timedelta(minutes=task_duration)).strftime("%Y-%m-%d %H:%M:%S")
    task_types = ["性能", "可靠性"]
    data = {
        "plan_start_time": plan_start_time.strftime("%Y-%m-%d %H:%M:%S"),
        "plan_end_time": plan_end_time,
        "experiment_id": random.choice(FIXED_EXPERIMENT_IDS),
        "laboratory": "CD",
        "task_id": str(random.randint(10 ** 10, 10 ** 11 - 1)),
        "task_sort": random.choice([1, 2, 3]),
        "task_type": random.choice(task_types),
        "task_order":random.randint(1,5),
        "task_duration": task_duration,
        "available_device": random.choice(FIXED_USING_DEVICE_RESULTS),
        "available_worker": random.choice(FIXED_USING_WORKER_RESULTS)
    }
    return data


def generate_random_data_list(num_samples=20, num_samples2=150):
    data_list = []
    data_list2 = []
    for _ in range(num_samples):
        data = generate_random_data()
        data_list.append(data)
    for _ in range(num_samples2):
        data = generate_random_data2()
        data_list2.append(data)
    return data_list, data_list2


# Generate and print 50 random data entries
random_data_list, random_data_list2 = generate_random_data_list()
with open('random_data1.json', 'w', encoding="utf-8") as json_file:
    json.dump(random_data_list, json_file,ensure_ascii=False)
with open('random_data2.json', 'w',encoding="utf-8") as json_file:
    json.dump(random_data_list2, json_file,ensure_ascii=False)
# Print the generated da

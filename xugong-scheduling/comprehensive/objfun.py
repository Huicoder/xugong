import datetime
import numpy as np
import pandas as pd


def process_data_set(data_set):
    device_map = dict()
    workder_map = dict()
    field_map = dict()
    field_map_indexstring = dict()
    workder_map_indexstring = dict()
    field_index = 0
    worker_index = 0
    lower = []
    upper = []
    for item in data_set["scheduled_task"]:
        tmp = []
        if list(item["using_field"].keys())[0] not in field_map:
            field_map[list(item["using_field"].keys())[0]] = str(field_index)
            field_map_indexstring[str(field_index)] = list(item["using_field"].keys())[
                0
            ]
            field_index += 1
        item["using_field"][field_map[list(item["using_field"].keys())[0]]] = item[
            "using_field"
        ][list(item["using_field"].keys())[0]]
        del item["using_field"][list(item["using_field"].keys())[0]]
        for worker_string in item["using_worker"]:
            if worker_string not in workder_map:
                workder_map[worker_string] = str(worker_index)
                workder_map_indexstring[str(worker_index)] = worker_string
                worker_index += 1
            tmp.append(workder_map[worker_string])
        item["using_worker"] = tmp
    for item in data_set["scheduling_task"]:
        if list(item["available_field"].keys())[0] not in field_map:
            field_map[list(item["available_field"].keys())[0]] = str(field_index)
            field_map_indexstring[str(field_index)] = list(
                item["available_field"].keys()
            )[0]
            field_index += 1
        item["available_field"][
            field_map[list(item["available_field"].keys())[0]]
        ] = item["available_field"][list(item["available_field"].keys())[0]]
        del item["available_field"][list(item["available_field"].keys())[0]]
        if "using_worker" in item:
            lower.append([0] * len(item["using_worker"]))
            tmp = []
            for worker_string in item["using_worker"]:
                if worker_string not in workder_map:
                    workder_map[worker_string] = str(worker_index)
                    workder_map_indexstring[str(worker_index)] = worker_string
                    worker_index += 1
                tmp.append(workder_map[worker_string])
            item["using_worker"] = tmp
        else:
            lower.append([0] * item["needed_worker"])
            tmp = []
            for worker_string in item["available_worker"]:
                if worker_string not in workder_map:
                    workder_map[worker_string] = str(worker_index)
                    workder_map_indexstring[str(worker_index)] = worker_string
                    worker_index += 1
                tmp.append(workder_map[worker_string])
            item["available_worker"] = tmp
        for i in item["available_equipment"]:
            if i not in device_map:
                device_map[i] = item["available_equipment"][i]
    lower = sum(lower, [])
    for i in range(len(lower)):
        upper.append([worker_index])
    upper = sum(upper, [])
    n_var = len(lower)
    return {
        "workerIndex2String": workder_map_indexstring,
        "field_map_indexstring": field_map_indexstring,
        "optimizationProblem": {
            "n_var": n_var,
            "lower": lower,
            "upper": upper,
            "dataSet": [data_set],
            "algoResultType": 0,
        },
    }


def cal_cartesian_coord_2(values):
    """
    集合做笛卡尔积
    :param values: [['ZJ070风速仪'], ['ZJ060钢卷尺/5m,ZJ061钢卷尺/5m']]
    :return:[['ZJ070风速仪', 'ZJ060钢卷尺/5m'], ['ZJ070风速仪', 'ZJ061钢卷尺/5m']]
    """
    # 通过*values==value1,value2,value3...的方式，
    # 将values中的每个一维数组作为参数传递给meshgrid()函数
    mesh = np.meshgrid(*values)
    cart = np.array(mesh).T.reshape(-1, len(values))
    return cart.tolist()


def pan(
    data,
    field,
    worker,
    time_dict,
    pop_dict,
    device,
    device_dict,
    start,
    end,
    pre,
    residue_filed,
    num_flag,
    init_time,
):
    """
    判断待排任务与固定任务是否冲突，时间冲突则进行调整
    :param data: 数据集
    :param field: 场地id
    :param worker: 当前待排任务所需人员
    :param time_dict: 每个场地的使用时间段
    :param pop_dict: 每个人员的占用时间段
    :param device: 所需手持设备的一个组合
    :param device_dict: 每个手持设备的占用时间段
    :param start: 待排任务的开始时间
    :param end: 待排任务的结束时间
    :param pre: 存放前置任务的开始时间和结束时间
    :param residue_filed: 当前待排任务占用的场地数量
    :param num_flag: 场地占用标志
    :param init_time: 数据集提供的开始时间
    :return: 该待排任务的解决冲突后的实际开始时间和结束时间
    """
    time = []  # 存放待排任务与固定任务的发生各种冲突的时间
    if pre:  # 当前待排任务有前置任务
        time.append(pre)  # 将前置任务加入
    duration = end - datetime.datetime.strptime(start, "%Y-%m-%d %H:%M:%S")  # 待排任务的实验时长
    for j in range(len(data["scheduled_task"])):  # 对固定任务遍历
        if (
            int(list(data["scheduled_task"][j]["using_field"].keys())[0]) == field
            and list(data["scheduled_task"][j]["using_field"].values())[0]
            == residue_filed
        ):  # 判断当前待排任务与固定任务的场地是否冲突
            time.append(
                [
                    data["scheduled_task"][j]["start_time"],
                    data["scheduled_task"][j]["end_time"],
                ]
            )
    for i in worker:  # 对当前待排任务所需人员遍历
        for j in range(len(data["scheduled_task"])):
            if str(int(i)) in data["scheduled_task"][j]["using_worker"]:  # 判断待排任务所需人员是否与固定任务冲突
                time.append(
                    [
                        data["scheduled_task"][j]["start_time"],
                        data["scheduled_task"][j]["end_time"],
                    ]
                )
        if i in pop_dict:  # 如果当前待排任务与前面排好的任务所需人员相同
            time.append(pop_dict[i][-1])  # 将该实验人员的占用的最后一个时间加入time
    for i in device:  # 对当前待排任务的手持设备进行遍历
        for j in range(len(data["scheduled_task"])):
            if i in data["scheduled_task"][j]["using_equipment"]:  # 判断待排任务的所需手持设备与固定任务是否冲突
                time.append(
                    [
                        data["scheduled_task"][j]["start_time"],
                        data["scheduled_task"][j]["end_time"],
                    ]
                )
        if i in device_dict:
            for kk in range(len(device_dict[i])):
                time.append(device_dict[i][kk])
    time.sort(key=lambda x: (x[0], x[1]))
    Flag = True
    while Flag:  # 循环结束标志
        flag = True
        start, end = judege(start, end, duration)  # 根据时间表设置该待排任务的开始和结束时间
        for i in range(len(time)):  # 解决时间冲突问题，将待排任务的位置固定下来
            if (time[i][0] < start < time[i][1]) or (
                time[i][0] < str(end) < time[i][1]
            ):
                start = time[i][1]
            elif str(end) <= time[i][0]:
                flag = False
            elif start <= time[i][0] and str(end) >= time[i][1]:
                start = time[i][1]
            start, end = judege(start, end, duration)  # 每解决一个时间冲突都需要根据时间表重新安排时间
            if not flag:  # 当flag为False时，说明时间冲突完全解决，退出循环
                break
        time1 = [[start, str(end)]]
        if not num_flag:  # 没有全部占用的时候，需要考虑同场地，全部占用的时候，已经避开了场地其他
            # 任务，只需要跟固定任务比即可
            if field in time_dict:
                time1 += time_dict[field]
        time1.sort(key=lambda x: (x[0], x[1]))
        for j in range(len(data["scheduled_task"])):
            if int(list(data["scheduled_task"][j]["using_field"].keys())[0]) == field:  # 待排任务与固定任务是否占用同一场地
                if int(list(data["scheduled_task"][j]["using_field"].values())[0]) != 1:  # 固定任务占用的场地数量不等于1？
                    continue
                #  t是固定任务的时间
                t = [
                    data["scheduled_task"][j]["start_time"],
                    data["scheduled_task"][j]["end_time"],
                ]
                if not attack(
                    time1,
                    t,
                    int(list(data["scheduled_task"][j]["using_field"].values())[0]),
                    residue_filed,
                ):
                    start = t[1]
                    end = (
                        datetime.datetime.strptime(start, "%Y-%m-%d %H:%M:%S")
                        + duration
                    )
                    break
        else:
            Flag = False
    return start, end


def judege(start, end, duration):
    if "02-16" < start[5:10] < "07-14":
        if (
            "08:30:00" <= start[11:] < "12:00:00"
            or "13:30:00" <= start[11:] < "18:00:00"
        ):
            start = start
        elif "12:00:00" <= start[11:] < "13:30:00":
            start = start[:11] + "13:30:00"
        elif "00:00:00" <= start[11:] < "08:30:00":
            start = start[:11] + "08:30:00"
        else:
            start = (
                str(
                    datetime.datetime.strptime(start[:10], "%Y-%m-%d")
                    + datetime.timedelta(days=1)
                )[:11]
                + "08:30:00"
            )
    elif "07-15" < start[5:10] < "09-30":
        if (
            "08:30:00" <= start[11:] < "12:00:00"
            or "14:00:00" <= start[11:] < "18:00:00"
        ):
            start = start
        elif "12:00:00" <= start[11:] < "14:00:00":
            start = start[:11] + "14:00:00"
        elif "00:00:00" <= start[11:] < "08:30:00":
            start = start[:11] + "08:30:00"
        else:
            start = (
                str(
                    datetime.datetime.strptime(start[:10], "%Y-%m-%d")
                    + datetime.timedelta(days=1)
                )[:11]
                + "08:30:00"
            )
    elif "10-01" < start[5:10] < "11-15":
        if (
            "08:30:00" <= start[11:] < "12:00:00"
            or "13:30:00" <= start[11:] < "18:00:00"
        ):
            start = start
        elif "12:00:00" <= start[11:] < "13:30:00":
            start = start[:11] + "13:30:00"
        elif "00:00:00" <= start[11:] < "08:30:00":
            start = start[:11] + "08:30:00"
        else:
            start = (
                str(
                    datetime.datetime.strptime(start[:10], "%Y-%m-%d")
                    + datetime.timedelta(days=1)
                )[:11]
                + "08:30:00"
            )
    else:
        if (
            "08:30:00" <= start[11:] < "12:00:00"
            or "13:30:00" <= start[11:] < "17:30:00"
        ):
            start = start
        elif "12:00:00" <= start[11:] < "13:30:00":
            start = start[:11] + "13:30:00"
        elif "00:00:00" <= start[11:] < "08:30:00":
            start = start[:11] + "08:30:00"
        else:
            start = (
                str(
                    datetime.datetime.strptime(start[:10], "%Y-%m-%d")
                    + datetime.timedelta(days=1)
                )[:11]
                + "08:30:00"
            )
    end = datetime.datetime.strptime(start, "%Y-%m-%d %H:%M:%S") + duration
    return start, end


def field_num(data_set):
    """
    统计场地数量
    :param data_set: 数据集
    :return: 返回场地数量
    """
    field = 0
    field_map = []
    for item in data_set["scheduling_task"]:
        if list(item["available_field"].keys())[0] not in field_map:
            field_map.append(list(item["available_field"].keys())[0])
            field += 1
    for item in data_set["scheduled_task"]:
        if list(item["using_field"].keys())[0] not in field_map:
            field_map.append(list(item["using_field"].keys())[0])
            field += 1
    return field


def attack(time, t, nums, residue_filed):
    """

    :param time: 待排任务解决时间冲突后的开始和结束时间
    :param t: 固定任务开始和结束时间
    :param nums: 固定任务占用的场地，取其数量
    :param residue_filed: 待排任务占用的场地，取其数量
    :return:
    """
    s = 0
    for i in range(len(time)):
        if (
            (time[i][0] < t[0] < time[i][1])
            or (time[i][0] < t[1] < time[i][1])
            or (t[0] <= time[i][0] and t[1] >= time[i][1])
        ):
            s += 1
    if s + nums <= residue_filed:
        return True
    else:
        return False


def run(x, data):
    data1 = data[0].copy()
    init_time = data1["schedule_start_time"]

    experiment_i = []  # 每个场地的设备情况
    pop_i = []  # 每个场地上人员情况
    field_nums = field_num(data1)  # 统计场地数量
    residue_filed = {}  # 占用场地
    # 统计分到同一个场地的任务（类似于传动实验室的“分组”）
    for i in range(field_nums):
        j = 0  # 场地下标从0开始
        nums = 0  # 第几个任务（任务id）
        people = []  # 记录每个任务所需人员
        experiment = []  # 记录任务id
        seq = []
        while j < len(x):
            if "available_worker" in data1["scheduling_task"][nums]:
                t = data1["scheduling_task"][nums]["available_worker"]  # 待排任务可用的人员编号
                j_ = data1["scheduling_task"][nums]["needed_worker"]  # 待排任务所需的人员数量
            else:
                t = data1["scheduling_task"][nums]["using_worker"]
                j_ = len(data1["scheduling_task"][nums]["using_worker"])
            if (
                int(list(data1["scheduling_task"][nums]["available_field"].keys())[0])
                == i  # 第i个场地
            ):  # 该待排任务占用该场地
                residue_filed[i] = data1["scheduling_task"][nums]["available_field"][
                    str(i)
                ]
                if "available_worker" in data1["scheduling_task"][nums]:
                    people.append(
                        list(x[j: j + data1["scheduling_task"][nums]["needed_worker"]])
                    )
                else:
                    people.append(data1["scheduling_task"][nums]["using_worker"])
                    # 将列表中的字符转为int类型
                    # people.append(list(map(int, data1["scheduling_task"][nums]["using_worker"])))

                if "plan_start_time" in data1["scheduling_task"][nums]:  # 该待排任务有计划开始时间
                    for h in range(j_):  # j_记录该任务所需人员个数，seq记录每个人员工作开始时间以及任务优先级
                        seq.append(
                            (
                                data1["scheduling_task"][nums]["plan_start_time"][0:7],
                                data1["scheduling_task"][nums]["task_order"],
                            )
                        )
                        experiment.append(nums)  # 该任务对应的设备id
                else:  # 没有计划开始时间的待排任务
                    for h in range(j_):
                        seq.append(
                            ("3000-13", data1["scheduling_task"][nums]["task_order"])
                        )
                        experiment.append(nums)
            j += j_  # 对下一个任务进行上述操作
            nums += 1  # 任务数加1
        people = sum(people, [])  # sum() 也可以用于列表的展开，效果相当于各子列表相加
        tmp = sorted(range(len(people)), key=lambda x: (seq[x][0], seq[x][1]))
        # people = np.array(people)[tmp]
        # experiment = np.array(experiment)[tmp]
        pop_i.append(np.array(people)[tmp])
        experiment_i.append(np.array(experiment)[tmp])
    experiment_i = pd.DataFrame(experiment_i)
    pop_i = pd.DataFrame(pop_i)

    time_dict = {}  # 场地时间
    pop_dict = {}  # 人员时间
    device_dict = {}  # 设备时间
    experiment_done = {}  # 判断实验是否做过
    Flag = {}  # 存放已做实验的时间(key值为task_id)
    Flag_i = [0] * field_nums  # 存放场地的第几个实验
    time_dict_i = {}  # 存放每个场地中最快结束和最慢结束的时间

    while len(experiment_done) != len(data1["scheduling_task"]):  # 确保所有的实验全部做完
        for j in range(experiment_i.shape[0]):  # 对场地进行遍历
            i = Flag_i[j]
            if not np.isnan(experiment_i[i][j]):
                if (
                    len(data1["scheduling_task"][int(experiment_i[i][j])]["pre_tasks"])
                    != 0
                ):  # 该待排任务有前置任务
                    if (
                        data1["scheduling_task"][int(experiment_i[i][j])]["pre_tasks"][
                            0
                        ]
                        not in Flag
                    ):  # 该待排任务的前置任务没有做过，需要跳过该待排任务
                        continue
                if i == 0:  # 先看每个场地的第一个实验
                    # 预估的结束时间
                    t = datetime.datetime.strptime(
                        init_time, "%Y-%m-%d %H:%M:%S"
                    ) + datetime.timedelta(
                        minutes=data1["scheduling_task"][int(experiment_i[i][j])][
                            "task_duration"
                        ]
                    )
                    start_time = init_time
                    if (
                        list(
                            data1["scheduling_task"][int(experiment_i[i][j])][
                                "needed_field"
                            ].values()
                        )[0]
                        == residue_filed[j]
                    ):  # 当前待排任务全部占用该场地
                        num_flag = True  # 该场地全部占用的标志
                    else:
                        num_flag = False
                else:  # 再看场地的第二个及以后的实验
                    #  实验做过了，要么出现在experiment_done中，要么experiment_done中为True
                    if (
                        experiment_i[i][j] in experiment_done
                        and experiment_done[int(experiment_i[i][j])]
                    ):
                        Flag_i[j] += 1
                        continue
                        # 如果当前项目需要的场地数全部占用，则从最晚结束时间开始
                    if (
                        list(
                            data1["scheduling_task"][int(experiment_i[i][j])][
                                "needed_field"
                            ].values()
                        )[0]
                        == residue_filed[j]
                    ):
                        num_flag = True
                        t = datetime.datetime.strptime(
                            time_dict_i[j]["end_slow"], "%Y-%m-%d %H:%M:%S"
                        ) + datetime.timedelta(
                            minutes=data1["scheduling_task"][int(experiment_i[i][j])][
                                "task_duration"
                            ]
                        )
                        start_time = time_dict_i[j]["end_slow"]
                    else:  # 如果没有全部占用，则从最早结束时间开始
                        num_flag = False
                        t = datetime.datetime.strptime(  # 待排任务的结束时间
                            time_dict_i[j]["end_fast"], "%Y-%m-%d %H:%M:%S"
                        ) + datetime.timedelta(
                            minutes=data1["scheduling_task"][int(experiment_i[i][j])][
                                "task_duration"
                            ]
                        )
                        start_time = time_dict_i[j]["end_fast"]  # 待排任务的开始时间
                Flag_i[j] += 1
                # 当前需要的人
                if "needed_worker" in data1["scheduling_task"][int(experiment_i[i][j])]:  # 有"need worker"字段
                    people1 = list(
                        pop_i.iloc[j][
                            i: i
                            + data1["scheduling_task"][int(experiment_i[i][j])][
                                "needed_worker"
                            ]
                        ]
                    )
                else:  # 没有"need worker"字段
                    people1 = data1["scheduling_task"][int(experiment_i[i][j])][
                        "using_worker"
                    ]
                # 当前待排任务所需的手持设备
                device = list(
                    data1["scheduling_task"][int(experiment_i[i][j])][
                        "needed_equipment"
                    ].keys()
                )
                device1 = []
                for h in range(len(device)):
                    device1 += [device[h].split(",")]
                device1 = cal_cartesian_coord_2(device1)  # 笛卡尔积
                time1 = []  # 存放不同手持设备组合的待排任务的开始和结束时间
                for h in device1:
                    #  前置实验的时间
                    if not data1["scheduling_task"][int(experiment_i[i][j])][
                        "pre_tasks"
                    ]:  # 当前待排任务没有前置任务
                        pre = []
                    else:  # 当前待排任务有前置任务
                        pre = Flag[
                            data1["scheduling_task"][int(experiment_i[i][j])][
                                "pre_tasks"
                            ][0]
                        ]
                    start1, end1 = pan(
                        data1,
                        j,
                        people1,
                        time_dict,
                        pop_dict,
                        h,
                        device_dict,
                        start_time,
                        t,
                        pre,
                        residue_filed[j],
                        num_flag,
                        init_time,
                    )
                    time1.append([start1, str(end1)])
                # 选取最快的组合方式
                # 更新设备表
                for h1 in list(device1[np.argmin(np.array(time1)[:, 1])]):
                    device_dict.setdefault(h1, []).append(
                        time1[np.argmin(np.array(time1)[:, 1])]
                    )
                # 更新人员表
                for h in people1:
                    pop_dict.setdefault(h, []).append(
                        time1[np.argmin(np.array(time1)[:, 1])]
                    )
                # 更新场地表
                time_dict.setdefault(j, []).append(
                    time1[np.argmin(np.array(time1)[:, 1])]
                )
                experiment_done[int(experiment_i[i][j])] = True
                Flag[
                    data1["scheduling_task"][int(experiment_i[i][j])]["task_id"]
                ] = time1[np.argmin(np.array(time1)[:, 1])]
                if len(time_dict[j]) < residue_filed[j]:  # 当场地上任务数小于容量时，开始时间就是初始时间。
                    end_fast = init_time
                else:
                    end_fast = np.array(time_dict[j])[
                        np.argmin(np.array(time_dict[j])[:, 1])
                    ][-1]
                end_slow = np.array(time_dict[j])[
                    np.argmax(np.array(time_dict[j])[:, 1])
                ][-1]
                time_dict_i[j] = {"end_fast": end_fast, "end_slow": end_slow}
    f = init_time
    for i in range(field_nums):
        if i in time_dict_i:
            if f < time_dict_i[i]["end_slow"]:
                f = time_dict_i[i]["end_slow"]
    f = datetime.datetime.strptime(f, "%Y-%m-%d %H:%M:%S") - datetime.datetime.strptime(
        init_time, "%Y-%m-%d %H:%M:%S"
    )
    f = f.days * 24 * 3600 + f.seconds  # 秒当作目标值
    return f


def main(x, data):
    return run(x, data)


if __name__ == '__main__':
    x = [3, 8, 3, 4, 5, 6]
    data_set = pd.read_json("D:\\WorkSpace\\plat-go\\xugong-scheduling\\试验场排程.json")['data']
    preprocessing_res = process_data_set(data_set)
    data = preprocessing_res["optimizationProblem"]["dataSet"]
    main(x, data)

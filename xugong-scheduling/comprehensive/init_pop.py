import random
import platgo as pg
import numpy as np
import pandas as pd


def process_data_set(data_set):
    device_map = dict()
    workder_map = dict()
    field_map = dict()
    field_map_indexstring = dict()
    workder_map_indexstring = dict()
    field_index = 0
    worker_index = 0
    lower = []
    upper = []
    for item in data_set["scheduled_task"]:
        tmp = []
        if list(item["using_field"].keys())[0] not in field_map:
            field_map[list(item["using_field"].keys())[0]] = str(field_index)
            field_map_indexstring[str(field_index)] = list(item["using_field"].keys())[
                0
            ]
            field_index += 1
        item["using_field"][field_map[list(item["using_field"].keys())[0]]] = item[
            "using_field"
        ][list(item["using_field"].keys())[0]]
        del item["using_field"][list(item["using_field"].keys())[0]]
        for worker_string in item["using_worker"]:
            if worker_string not in workder_map:
                workder_map[worker_string] = str(worker_index)
                workder_map_indexstring[str(worker_index)] = worker_string
                worker_index += 1
            tmp.append(workder_map[worker_string])
        item["using_worker"] = tmp
    for item in data_set["scheduling_task"]:
        if list(item["available_field"].keys())[0] not in field_map:
            field_map[list(item["available_field"].keys())[0]] = str(field_index)
            field_map_indexstring[str(field_index)] = list(
                item["available_field"].keys()
            )[0]
            field_index += 1
        item["available_field"][
            field_map[list(item["available_field"].keys())[0]]
        ] = item["available_field"][list(item["available_field"].keys())[0]]
        del item["available_field"][list(item["available_field"].keys())[0]]
        if "using_worker" in item:
            lower.append([0] * len(item["using_worker"]))
            tmp = []
            for worker_string in item["using_worker"]:
                if worker_string not in workder_map:
                    workder_map[worker_string] = str(worker_index)
                    workder_map_indexstring[str(worker_index)] = worker_string
                    worker_index += 1
                tmp.append(workder_map[worker_string])
            item["using_worker"] = tmp
        else:
            lower.append([0] * item["needed_worker"])
            tmp = []
            for worker_string in item["available_worker"]:
                if worker_string not in workder_map:
                    workder_map[worker_string] = str(worker_index)
                    workder_map_indexstring[str(worker_index)] = worker_string
                    worker_index += 1
                tmp.append(workder_map[worker_string])
            item["available_worker"] = tmp
        for i in item["available_equipment"]:
            if i not in device_map:
                device_map[i] = item["available_equipment"][i]
    lower = sum(lower, [])
    for i in range(len(lower)):
        upper.append([worker_index])
    upper = sum(upper, [])
    n_var = len(lower)
    return {
        "workerIndex2String": workder_map_indexstring,
        "field_map_indexstring": field_map_indexstring,
        "optimizationProblem": {
            "n_var": n_var,
            "lower": lower,
            "upper": upper,
            "dataSet": [data_set],
            "algoResultType": 0,
        },
    }


def main(self, N=None):
    if N is None:
        N = self.pop_size
    # data1 = self.data[0]
    data_set = pd.read_json("D:\\WorkSpace\\plat-go\\xugong-scheduling\\试验场排程.json")['data']
    preprocessing_res = process_data_set(data_set)
    data1 = preprocessing_res["optimizationProblem"]["dataSet"][0]
    dec = []
    for k in range(N):
        dec1 = []
        for i in range(len(data1["scheduling_task"])):
            if "available_worker" in data1["scheduling_task"][i]:
                pop_i = random.sample(
                    data1["scheduling_task"][i]["available_worker"],
                    k=data1["scheduling_task"][i]["needed_worker"],
                )  # 随机抽取K个不重复的元素形成新的序列,k值不能超出序列的元素个数
            else:
                pop_i = data1["scheduling_task"][i]["using_worker"]
            dec1.extend(pop_i)
        dec += [dec1]
    dec = np.array(dec, dtype=np.float64)
    return pg.Population(decs=dec)


if __name__ == '__main__':
    main(pg.problems.TSP(), 10)

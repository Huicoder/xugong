from .multi_objective.Real_world_MOPs.Sparse_SR import Sparse_SR  # noqa: F401
from .multi_objective.Real_world_MOPs.Sparse_CD import Sparse_CD  # noqa: F401
from .multi_objective.transmission2 import XugongTransmission2
from .single_objective.xugong.transmission import (  # noqa: F401
    XugongTransmission,
)  # noqa: F401
from .single_objective.xugong.comprehensive import (  # noqa: F401
    XugongComprehensive,
)  # noqa: F401
